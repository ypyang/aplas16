# Coq formalization of λI

This is the Coq formalization of `λI` for both variants and the erased
system. The scripts are based on Arthur Charguéraud's formalization of
Calculus of Constructions[^1]. Coq files are located at
[coq/](coq/). The corresponding type systems of Coq files are
described as follows
(files containing main results are listed in parentheses):

* `WeakCast_*.v`: `λIw` with weak casts 
  ([decidability](coq/WeakCast_Decidability.v), 
   [soundness](coq/WeakCast_Soundness.v))
* `FullCast_*.v`: `λIp` with full casts 
  ([decidability](coq/FullCast_Decidability.v), 
   [soundness](coq/FullCast_Soundness.v))
* `CoCMu_*.v`: Erased system of `λIp` 
  ([soundness](coq/CoCMu_Soundness.v), 
   [parallel reduction](coq/CoCMu_ParaReduction.v))
* `tlc/`: TLC Coq library[^2] by Arthur Charguéraud

Main theorems of metatheory, i.e., decidability of type checking and
type safety are located in `*_Decidability.v` and `*_Soundness.v`,
respectively.

To compile all scripts, run `make`. Tested on Coq v8.5pl1.

[^1]: http://www.chargueraud.org/softs/ln/
[^2]: http://www.chargueraud.org/softs/tlc/

