# Unified Syntax with Iso-Types
Extended version and online materials

---------

## Extended version 
See [isotype-extended.pdf](isotype-extended.pdf) ([slides](slides.pdf)). 
Extra materials are provided in the Appendix section.

## Coq formalization of proofs
See [README-coq.md](README-coq.md).
Files are located at [coq/](coq/).

## Implementation of λI
See [README-impl.md](README-impl.md).
Files are located at [impl/](impl/).
Try in-browser compiler [online](https://fun-lang.github.io/).

## Download the repository
[Link](https://bitbucket.org/ypyang/aplas16/downloads)
