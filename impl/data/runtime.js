function $getRaw(o, s) {
    if (o !== null && typeof o === "object" && o.hasOwnProperty("raw")) {
        return o.raw;
    } else {
        if (s === true) {
            return JSON.stringify(o);
        } else {
            return o;
        }
    }
}

function $var(v) {
    if (typeof v === "function") {
        return v();
    } else {
        return v;
    }
}

function $genExpr(e) {
    var v = $runExpr(e);
    var gen = new $Expr(v);
    if (typeof v !== "function") {
        gen.evaled = true;
    }
    return gen;
}

function $runExpr(expr){
    while (expr instanceof $Expr) {
        expr = expr.run();
    }
    return expr;
}

function $Expr(v) {
    this.evaled = false;
    this.args = [];
    this.value = v;
}

$Expr.prototype.app = function () {
    this.args.push(arguments);
    return this;
};


$Expr.prototype.val = function (v) {
    this.evaled = true;
    this.value = v;
    return this;
};

$Expr.prototype.ev = function () {
    var val = $runExpr(this);
    if (typeof val === "function") {
        return val.toString();
    } else if (typeof val === "string") {
        return JSON.stringify(val);
    } else if (val === null) {
        return "()";
    } else {
        return $getRaw(val, false);
    }
};

$Expr.prototype.run = function () {
    if (this.evaled) {
        return this.value;
    } else {
        if (this.args.length > 0) {
            this.value = this.value.apply(null, this.args[0]);
            if (this.value instanceof $Expr) {
                this.value.args = this.value.args.concat(this.args.slice(1));
            }
        }
        this.evaled = true;
        return this.value;
    }
};

var $Op = {
    show: function () {
        return new $Expr(function (x) {
            var val = $runExpr(x);
            if (typeof val === "function") {
                return JSON.stringify(val.toString());
            } else if (val === null) {
                return JSON.stringify("()");
            } else {
                return $getRaw(val, true);
            }
        });
    },
    neg: function () {
        return new $Expr(function (x) {
            return (-$runExpr(x));
        });
    },
    plus: function () {
        return new $Expr(function (x, y) {
            return ($runExpr(x) + $runExpr(y)) || 0;
        });
    },
    minus: function () {
        return new $Expr(function (x, y) {
            return ($runExpr(x) - $runExpr(y));
        });
    },
    mul: function () {
        return new $Expr(function (x, y) {
            return ($runExpr(x) * $runExpr(y));
        });
    },
    concat: function () {
        return new $Expr(function (x, y) {
            return ($runExpr(x) + $runExpr(y)) || "";
        });
    },
    ifthenelse: function () {
        return new $Expr(function (x, y, z) {
            if ($runExpr(x) === true) {
                return y;
            } else {
                return z;
            }
        });
    },
    not: function () {
        return new $Expr(function (x) {
            return $runExpr(x) !== true;
        });
    },
    grt: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) > $runExpr(y);
        });
    },
    grteq: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) >= $runExpr(y);
        });
    },
    less: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) < $runExpr(y);
        });
    },
    lesseq: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) <= $runExpr(y);
        });
    },
    eq: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) === $runExpr(y);
        });
    },
    neq: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) !== $runExpr(y);
        });
    },
    and: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) && $runExpr(y);
        });
    },
    or: function () {
        return new $Expr(function (x, y) {
            return $runExpr(x) || $runExpr(y);
        });
    },
    fix: function () {
        return new $Expr(function (f) {
            return f().app(function() {
                return $Op.fix().app(f);
            });
        });
    }
};
