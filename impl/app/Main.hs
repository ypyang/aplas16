{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE CPP #-}
module Main where

import           Control.Exception            (SomeException, try)
import           Control.Monad.State.Strict
import qualified Data.ByteString.Char8        as BS
import           Data.FileEmbed
import           Data.List                    (isPrefixOf)
import           Data.Maybe                   (fromMaybe)
import           Language.Fun
import           Language.Fun.Pretty
import           Prelude                      hiding ((<$>))
import           System.Console.GetOpt
import           System.Console.Repline
import           System.Environment
import           System.Exit
import           System.FilePath              (dropExtension)
import           System.IO                    (hPutStrLn, stderr)
import           System.Process               (readProcessWithExitCode)
import           Text.PrettyPrint.ANSI.Leijen hiding (Pretty)

#ifdef __GHCJS__
import GHCJS.Marshal
import GHCJS.Foreign hiding (Object)
import GHCJS.Foreign.Callback
import GHCJS.Types
import Data.JSString (JSString, pack, unpack)
import JavaScript.Object
import JavaScript.Object.Internal (Object(..))
#else

data ReplState = ReplState {
  replDebug      :: Bool,
  replWriteDebug :: Bool,
  replCtx        :: Ctx
  }

initState :: ReplState
initState = ReplState {
  replDebug = True,
  replWriteDebug = False,
  replCtx = emptyCtx
  }

type Repl a = HaskelineT (StateT ReplState IO) a

defaultMatcher :: MonadIO m => [(String, CompletionFunc m)]
defaultMatcher = map (\(s, _) -> (':' : s, fileCompleter)) $
                 filter (\(s, _) -> case s of
                           ('l':_) -> True
                           _ -> False)
                 options

defaultCmd :: String -> Repl ()
defaultCmd = interpretStr StageEval

optionsWithHelp :: [(String, [String] -> Repl (), String)]
optionsWithHelp = [
  ("help", help, "Show this text"),
  ("?", help, "Show help"),
  ("quit", quit, "Quit interactive"),
  ("load", load defaultCmd, "Load file and evaluate"),
  ("lraw", load $ interpretStr StageRaw, "Load file and print AST"),
  ("lprint", load $ interpretStr StagePretty, "Load file and pretty print"),
  ("ldesugar", load $ interpretStr StageDesugar, "Load file and desugar"),
  ("ltype", load $ interpretStr StageTypecheck, "Load file and infer the type"),
  ("ltrans", load $ interpretStr StageTrans, "Load file and translate to core"),
  ("lcore", load $ interpretStr StageTypeCore, "Load file and typecheck core"),
  ("raw", interpret StageRaw, "Print the abstract syntax tree"),
  ("print", interpret StagePretty, "Pretty print the input"),
  ("desugar", interpret StageDesugar, "Print desugared results"),
  ("type", interpret StageTypecheck, "Infer the type of expression"),
  ("trans", interpret StageTrans, "Typecheck translated core"),
  ("core", interpret StageTypeCore, "Show translated core context and expression"),
  ("reset", reset, "Reset the context"),
  ("context", context, "Print the context"),
  ("debug", debug, "Toggle debugging"),
  ("log", logdebug, "Write debug logging file (very slow)")
  ]

options :: [(String, [String] -> Repl ())]
options = map (\(a, b, _) -> (a, b)) optionsWithHelp

byWord :: Monad m => WordCompleter m
byWord n = do
  let names = map (\(n', _) -> ':':n') options
  return $ filter (isPrefixOf n) names

ini :: Repl ()
ini = putMsg $ verStr ++ ", :? for help"

help :: [String] -> Repl ()
help _ = putMsg . unlines $
  "Usage:" : helpText

helpText :: [String]
helpText = map (\(c, _, h) ->
                 " :" ++ c ++
                 replicate (10 - length c) ' ' ++ h) optionsWithHelp

quit :: [String] -> Repl ()
quit _ = putMsg "Ctrl-D to quit."

putCtx :: ReplState -> Repl ()
putCtx = lift . put

getCtx :: Repl ReplState
getCtx = lift get

putMsg :: String -> Repl ()
putMsg = liftIO . putStrLn

ppMsg :: Doc -> Repl ()
ppMsg d = liftIO $ putDoc (d <> line)

readTry :: IO String -> Repl (Either SomeException String)
readTry = liftIO . try

load :: (String -> Repl ()) -> [String] -> Repl ()
load cmd inp = do
  let path = unwords inp
  ret <- readTry $ readFile path
  case ret of
    Left err -> ppMsg $ warn "Load file error" <+> text (show err)
    Right contents -> do
      ppMsg $ info path
      resetCtx
      cmd contents

resetCtx :: Repl ()
resetCtx = do
  st <- getCtx
  putCtx ReplState {
    replCtx = emptyCtx,
    replDebug = replDebug st,
    replWriteDebug = replWriteDebug st
    }

reset :: [String] -> Repl ()
reset _ = do
  resetCtx
  putMsg "Context cleaned"

debug :: [String] -> Repl ()
debug _ = do
  st <- getCtx
  putCtx ReplState {
    replCtx = replCtx st,
    replDebug = not (replDebug st),
    replWriteDebug = replWriteDebug st
    }
  putMsg $ "Debug " ++ if replDebug st then "off" else "on"

logdebug :: [String] -> Repl ()
logdebug _ = do
  st <- getCtx
  putCtx ReplState {
    replCtx = replCtx st,
    replDebug = replDebug st,
    replWriteDebug = not (replWriteDebug st)
    }
  putMsg $ "Logging " ++ if replWriteDebug st then "off" else "on"

context :: [String] -> Repl ()
context _ = do
  st <- getCtx
  let ctx = replCtx st
  ppMsg $ ppCoreCtx (coreCtx ctx)

interpretStr :: Stage -> String -> Repl ()
interpretStr stage s = interpret stage [s]

interpret :: Stage -> [String] -> Repl ()
interpret stage str =
  unless (null str) $
  case parse (unwords str) of
    Left err ->
      ppMsg $ warn "Syntax error" <+> text err
    Right prog -> do
      st <- getCtx
      let repldebug = replDebug st
          replwritedbg = replWriteDebug st
          (result, dbg) = dump (if repldebug then DumpDebug else DumpNoDebug)
            stage (replCtx st) prog
          writeDbg = when (repldebug && replwritedbg) (liftIO $ writeDbgFile "fun-repl" dbg)
      case result of
        Left d -> ppMsg d >> writeDbg
        Right (ctx, e, ty) -> do
          ppMsg $ resDoc (pp e)
          when (stage == StageEval) (ppMsg $ colon <+> blue (pp ty))
          putCtx ReplState {
            replCtx = ctx,
            replDebug = repldebug,
            replWriteDebug = replwritedbg
            }
          writeDbg

#endif

data Flag = Version | Help | Debug | Run  | Haskell | Input String | Output String
  deriving (Eq, Show)

flags :: [OptDescr Flag]
flags = [
  Option ['h'] ["help"]    (NoArg Help)               "show this help",
  Option ['d'] ["debug"]   (NoArg Debug)              "compile with debugging info",
  Option ['s'] ["hask"]    (NoArg Haskell)            "compile to Haskell (default JavaScript)",
  Option ['r'] ["run"]     (NoArg Run)                "run with node or ghc (should be in PATH)",
  Option ['v'] ["version"] (NoArg Version)            "show version info",
  Option ['i'] ["input"]   (OptArg makeOutput "FILE") "input source file (or without -i)",
  Option ['o'] ["output"]  (OptArg makeOutput "FILE") "output javascript file (optional)"
  ]

makeOutput :: Maybe String -> Flag
makeOutput ms = Output (fromMaybe "" ms)

exitSucc :: Bool -> String -> IO ()
exitSucc ok s = do
  hPutStrLn stderr s
  exitWith $ if ok then ExitSuccess else ExitFailure 1

parseArgs :: [String] -> IO ()
parseArgs args =
  case getOpt RequireOrder flags args of
    (opts, nonOpts, [])
      | Help `elem` opts -> exitSucc True (usageInfo header flags)
      | Version `elem` opts -> exitSucc True verStr
      | otherwise -> do
          let isDebug = Debug `elem` opts
              isRun = Run `elem` opts
              isHask = Haskell `elem` opts
              fileExt = if isHask then ".hs" else ".js"
              findInput [] = ""
              findInput (x:xs) = case x of
                Input f -> f
                _ -> findInput xs
              findOutput [] = ""
              findOutput (x:xs) = case x of
                Output f -> f
                _ -> findInput xs
              inputArg = findInput opts
              inputPath
                | not (null inputArg) = inputArg
                | not (null nonOpts) = head nonOpts
                | otherwise = ""
              outputArg = findOutput opts
              outputPath = if null outputArg then
                             dropExtension inputPath ++ fileExt else outputArg
          if null inputPath then do
            ppIO $ warn "missing arguments" <> text "input filename is empty"
            exitWith (ExitFailure 1)
            else compile isDebug isRun isHask inputPath $
                 if inputPath == outputPath then inputPath ++ fileExt else outputPath
    (_, _, msgs)   ->
      exitSucc False $ concat msgs ++ usageInfo header flags
  where header = "To start the REPL, run the program without parameters.\n" ++
          "Usage: fun [OPTIONS] [FILE]"

ppIO :: Doc -> IO ()
ppIO d = putDoc (d <> line)

readTryIO :: IO String -> IO (Either SomeException String)
readTryIO = try

writeTryIO :: IO () -> IO (Either SomeException ())
writeTryIO = try

safeReadFile :: FilePath -> IO (Either Doc String)
safeReadFile path = do
  ret <- readTryIO $ readFile path
  case ret of
    Left err -> return $ Left (warn "Read file error" <+> text (show err))
    Right contents -> return $ Right contents

safeWriteFile :: FilePath -> String -> IO ()
safeWriteFile path str = do
  ret <- writeTryIO $ writeFile path str
  case ret of
    Left err -> ppIO $ warn "Write file error" <+> text (show err)
    _ -> return ()

dbgFile :: String -> FilePath
dbgFile s = s ++ "-debug.log"

writeDbgFile :: String -> [Doc] -> IO ()
writeDbgFile prefix dbgDocs = do
  let dbgText = show $ plain (vsep dbgDocs)
  safeWriteFile (dbgFile prefix) dbgText
  putStrLn $ "Debugging log written to " ++ (dbgFile prefix)

jsRuntime :: BS.ByteString
jsRuntime = $(embedFile "data/runtime.min.js")

jsRuntimeDebug :: BS.ByteString
jsRuntimeDebug = $(embedFile "data/runtime.js")

runJS :: Bool -> FilePath -> IO ()
runJS hask path = do
  let binary = if hask then "runghc" else "node"
  (ret, output, err) <- readProcessWithExitCode binary [path] ""
  if ret /= ExitSuccess then putStr err
    else putStr output

compile :: Bool -> Bool -> Bool -> FilePath -> FilePath -> IO ()
compile usedbg run hask inputPath outputPath = do
  inputRet <- safeReadFile inputPath
  case inputRet of
    Left err -> ppIO err
    Right inputStr -> do
      let runtimeStr = BS.unpack (if usedbg then
            jsRuntimeDebug else jsRuntime)
          (result, dbg, comment) =
            if hask then genhs usedbg inputStr
            else genjs usedbg runtimeStr inputStr
                 (\d -> text "console.log" <> parens d)
      case result of
        Left msg -> ppIO msg >> exitWith (ExitFailure 1)
        Right (output, _) -> do
          safeWriteFile outputPath $
            if usedbg then comment ++ output else output
          if run then runJS hask outputPath else do
            putStrLn $ "Compiled to " ++ outputPath
            when usedbg (writeDbgFile (dropExtension outputPath) dbg)

version :: IO ()
version = putStrLn verStr

verStr :: String
verStr = "Fun, version 0.1"

repl :: IO ()
#ifdef __GHCJS__
repl = do
  version
  putStrLn "Warning: REPL is not implemented for GHCJS yet"

compileFun :: String -> Either String (String, String)
compileFun input =
    let runtime = BS.unpack jsRuntime
        (result, _, _) = genjs False runtime input id
    in case result of
        Left msg -> Left $ show msg
        Right output -> Right output

setStringProp :: String -> JSString -> Object -> IO ()
setStringProp n s o = do
    v <- toJSVal s
    setProp (pack n) v o

funToJS :: JSVal -> IO ()
funToJS val = do
    let obj = Object val
    field <- getProp (pack "ret") obj
    fieldM <- fromJSVal field
    case fieldM of
      Just src -> do
        let input = unpack src
            result = compileFun input
        case result of
          Left msg -> setStringProp "err" (pack msg) obj
          Right (oret, otype) -> do
            setStringProp "ret" (pack oret) obj
            setStringProp "type" (pack otype) obj
      _ -> setStringProp "err" (pack "FunToJS: fail to get source") obj

foreign import javascript unsafe "funToJS_ = $1"
  jsInit :: Callback a -> IO ()

#else
repl = flip evalStateT initState $
       evalRepl "Fun> " defaultCmd options (Prefix (wordCompleter byWord) defaultMatcher) ini
#endif

main :: IO ()
main = do
#ifdef __GHCJS__
  callback <- syncCallback1 ThrowWouldBlock funToJS
  jsInit callback
#endif
  args <- getArgs
  if null args then repl
    else parseArgs args
