module Language.Fun (
  dump,
  genjs,
  genhs,
  evalFile,
  Result,
  parse,
  Ctx,
  emptyCtx,
  Stage(..),
  ctxMerge,
  ppCoreCtx,
  Debug(..),
  coreCtx,
  ) where

import           Control.Exception                (SomeException, try)
import           Data.Char                        (isSpace)
import qualified Data.LinkedHashMap.IntMap        as HM
import qualified Language.Fun.CodeGen             as G
import qualified Language.Fun.GenJS               as GJS
import qualified Language.Fun.GenHS               as GHS
import qualified Language.Fun.Core                as C
import qualified Language.Fun.Desugar             as D
import           Language.Fun.Parser.Parser       (parseString)
import           Language.Fun.Pretty
import qualified Language.Fun.Src                 as S
import qualified Language.Fun.Trans               as E
import qualified Language.Fun.Typecheck           as T
import           Prelude                          hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen     hiding (Pretty)
import           Unbound.Generics.LocallyNameless hiding (bind, subst)

type Result = (Either Doc (Ctx, C.Tm, S.Tm), [Doc])
type Ctx = (E.Ctx, T.Ctx, E.Cache)
data Debug = DumpDebug | DumpNoDebug deriving (Eq, Show)

emptyCtx :: Ctx
emptyCtx = (E.emptyCtx, T.emptyCtx, E.emptyCache)

coreCtx :: Ctx -> T.Ctx
coreCtx (_, c, _) = c

ctxMerge :: Ctx -> Ctx -> Ctx
ctxMerge (d1, c1, ca1) (d2, c2, ca2) =
  (E.ctxMerge d1 d2, T.ctxMerge c1 c2, E.cacheMerge ca1 ca2)

ppCoreCtx :: T.Ctx -> Doc
ppCoreCtx corectx =  info "Context" <$> strvsep ";"
  (map (\(v, e) -> keyword "def" <+>
                   resDoc (C.vardoc v) <+> text "=" <+> pp e)
   (HM.toList $ T.bndCtx corectx))

parse :: String -> Either String S.Prog
parse = parseString

ret :: Doc -> Result
ret d = (Left d, [])

retdbg :: Doc -> [Doc] -> Result
retdbg d b = (Left d, b)

retexpr :: Doc -> [Doc] -> Result
retexpr d b = (Left (resDoc d), b)

retres :: Ctx -> C.Tm -> S.Tm -> [Doc] -> Result
retres c e d b = (Right (c, e, d), b)

data Stage = StageRaw | StagePretty | StageDesugar
           | StageTypecheck | StageTrans | StageCore
           | StageTypeCore | StageEval
           deriving (Eq, Show)

readTry :: IO String -> IO (Either SomeException String)
readTry = try

parseExpectedOutput :: String -> Maybe String
parseExpectedOutput source =
  let firstLine = takeWhile (/= '\n') source in
  case firstLine of
    '-':'-':'>':r -> Just $ strip r
    _             -> Nothing

strip :: String -> String
strip = f . f
  where f = reverse . dropWhile isSpace

eval :: Debug -> Stage -> String -> Result
eval dbg stage inp = case parseString inp of
  Left err -> ret $ warn "Syntax error" <+> text err
  Right prog -> dump dbg stage emptyCtx prog

evalFile :: FilePath -> IO ((Doc, Maybe Doc), Bool)
evalFile path = do
  msg <- readTry $ readFile path
  let failed d = return ((d, Nothing), False)
      failWith d d' = return ((d, Just d'), False)
      succed d = return ((d, Nothing), True)
  case msg of
    Left err -> failed $ warn "Load file error" <+> text (show err)
    Right contents ->
      let (value, _) = eval DumpDebug StageEval contents in
      case value of
        Left err -> failed err
        Right (_ ,tm, _) -> case parseExpectedOutput contents of
          Nothing -> failed $ warn "No expectation" <+> pp tm
          Just expinp -> let prettyTm = pp tm in
            if (show . plain $ pp tm) == expinp then succed prettyTm
            else failWith prettyTm (text expinp)
          -- TODO: Using naive string comparison, should be improved by parsing:
            {- let (expect, _) = eval StageCore expinp in
              case expect of
                Left d -> failed $ align (warn "Illegal expectation" <+> resDoc (text expinp) <$> d)
                Right (_, tm') -> if tm `aeq` tm' then succed (pp tm)
                                  else failWith (pp tm) (pp tm') -}

dump :: Debug -> Stage -> Ctx -> S.Prog -> Result
dump dumpdbg stage (ictx, icorectx, icache) inp =
  if stage == StageRaw then ret (ppraw inp) else
    if stage == StagePretty then ret (pp inp) else
      case D.runM (D.desugarProg inp) of
        Left err -> ret $ warn "Desugar error" <+> err
        Right desugared ->
          if stage == StageDesugar then ret (pp desugared) else
            let ((trans, transdbg), cache) = E.runM icache (E.transProg ictx desugared) in
            case trans of
              Left err -> retdbg (warn "Type error" <+> err) transdbg
              Right (tctx, ty, tcorectx, coretm) ->
                let tydoc = pp ty
                    corectx = T.ctxMerge icorectx tcorectx
                    ctx = E.ctxMerge ictx tctx
                    ctxDoc c = (if T.isEmptyCtx corectx then empty
                                    else ppCoreCtx corectx <> line) <>
                          info "Expression" <$> pp coretm in
                if stage == StageTypecheck then retexpr tydoc transdbg else
                  if stage == StageTrans then retdbg (ctxDoc corectx) transdbg else
                    let evalCore = let (ev, evaldbg) = T.runM (T.reduceInf corectx coretm) in
                          case ev of
                            Left err -> retdbg (warn "Runtime error" <+> err) evaldbg
                            Right value -> retres (ctx, corectx, cache) value ty evaldbg
                    in if stage == StageEval && dumpdbg == DumpNoDebug then evalCore else
                         let convert = if dumpdbg == DumpDebug then T.ctxKeepBnd else id
                             tcctx = convert corectx
                             (core, tcdbg) = T.runM (T.typeCheck tcctx coretm) 
                             coredbg = ctxDoc tcctx : info "Traceback" : tcdbg in
                         case core of
                           Left err -> retdbg (warn "Core error" <+> err) coredbg
                           Right corety ->
                             if stage == StageCore then retres (ctx, corectx, cache) coretm ty coredbg else
                               if stage == StageTypeCore then retres (ctx, corectx, cache) corety ty coredbg else
                                 evalCore

genjs :: Bool -> String -> String -> (Doc -> Doc) -> (Either Doc (String, String), [Doc], String)
genjs isdebug predef rawinp wrapper =
  let dumpdebug = if isdebug then DumpDebug else DumpNoDebug
      (result, dbg) = eval dumpdebug StageCore rawinp in
  case result of
    Left err -> (Left err, dbg, [])
    Right ((_, ctx, _), coretm, ty) ->
      let (defs, tm, comm) = G.runM (GJS.jsGen ctx coretm)
      in (Right (show (plain (text predef <> defs <>
                              wrapper
                              (tm <> dot <> text "ev()") <> char ';')),
                 show . plain . pp $ ty),
          dbg, show $ plain comm)

genhs :: Bool -> String -> (Either Doc (String, String), [Doc], String)
genhs isdebug rawinp =
  let dumpdebug = if isdebug then DumpDebug else DumpNoDebug
      (result, dbg) = eval dumpdebug StageCore rawinp in
  case result of
    Left err -> (Left err, dbg, [])
    Right ((_, ctx, _), coretm, ty) ->
      let (defs, tm, comm) = G.runM (GHS.hsGen ctx coretm)
          tyDoc = pp ty
          tmTyped = case ty of
            S.LTy _ lty -> text "print" <+> parens (tm <+> text "::" <+> tyDoc)
            S.Any{} -> text "print undefined"
            S.Unit{} -> text "print ()"
            _ -> text "putStrLn \"<function>\""
      in (Right (show (plain (defs <$>
                              text "main" <+> equals <+> tmTyped <> line)),
                 show . plain . pp $ ty),
          dbg, show $ plain comm)
