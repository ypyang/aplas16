{
module Language.Fun.Parser.Lexer (
  Token(..),
  TokenClass(..),
  lexer,
  showPosn,
  Alex(..),
  alexError,
  runAlex,
  AlexPosn(..),
  ) where

import Data.Char (chr, isHexDigit, isOctDigit)
import Numeric (readOct)
}

%wrapper "monad"

$white = [\ \t\n\r]
$alpha = [a-zA-Z\_]
$digit = 0-9
$idchar = [$alpha $digit \']

@unit = "()"
$paren = [\(\)\[\]\{\}]
$symbol = [$paren \*\_\,\;]
$opchar = [\!\$\%\&\+\.\<\=\>\?\^\|\-\~]
$symchar = [$opchar \:]

@keyword = data | def | defrec | type
         | let | letrec | in
         | case | ret | of
         | castup | castdn | castup\! | castdn\!
         | Int | String
         | Bool | True | False
         | if | else | then
         | module | import | as | explicit

@compop = \\ \~ | "++" | "**" | "->" | "=>"
        | "==" | "/=" | ">=" | "<="
        | "&&" | "||" | "::"

@op = \\ | "/" | ":"
    | "." | "=" | "$"
    | "~" | "|" | "^"
    | "+" | "-" | "@"
    | ">" | "<" | "!"
    | \#

$octdig     = [0-7]
$hexdig     = [0-9A-Fa-f]
@octEscape  = [0123]? $octdig{1,2}
@hexEscape  = u $hexdig{4}
@charEscape = \\ (@octEscape | @hexEscape | [btnfr\"\'\\])
@gap     = \\ $white+ \\
@string  = . # [\"\\] | " " | @charEscape | @gap
@id      = $alpha $idchar*

tokens :-

<0> $white+             { skip }
<0> "--".*              { skip }
"{-"                    { nested_comment }
<0> @unit               { mkT (const TSym) }
<0> $symbol             { mkT (const TSym) }

<0> @keyword            { mkT (const TKey) }
<0> @id ("." @id)*      { mkT TVar }
<0> @compop             { mkT (const TSym) }
<0> @op                 { mkT (const TSym) }
<0> \: $symchar*        { mkT TOpSym }
<0> $digit+             { mkT (TInt . read) }
<0> \" @string* \"      { mkT (TStr . convChar . tail . init) }

{

data Token = T AlexPosn TokenClass String deriving (Show, Eq)

data TokenClass
  = TVar String
  | TInt Int
  | TStr String
  | TSym
  | TOpSym String
  | TKey
  | TEOF
  deriving (Show, Eq)

mkT :: (String -> TokenClass) -> AlexInput -> Int -> Alex Token
mkT c (p,_,_,str) len = return (T p (c inp) inp)
  where inp = take len str

nested_comment :: AlexInput -> Int -> Alex Token
nested_comment _ _ = do
  input <- alexGetInput
  go 1 input
  where go 0 input = do alexSetInput input; alexMonadScan
        go n input = do
          case alexGetByte input of
            Nothing  -> err input
            Just (c,input) -> do
              case chr (fromIntegral c) of
                '-' -> do
                  case alexGetByte input of
                    Nothing  -> err input
                    Just (125,input) -> go (n-1) input
                    Just (c,input)   -> go n input
                '\123' -> do
                  case alexGetByte input of
                    Nothing  -> err input
                    Just (c,input) | c == fromIntegral (ord '-') -> go (n+1) input
                    Just (c,input)   -> go n input
                c -> go n input
        err input = do alexSetInput input; lexError "error in nested comment"

lexicalError :: String -> a
lexicalError = error . ("lexical unescape error: " ++)

-- Converts a sequence of (unquoted) Java character literals, including
-- escapes, into the sequence of corresponding Chars. The calls to
-- 'lexicalError' double-check that this function is consistent with
-- the lexer rules for character and string literals. This function
-- could be expressed as another Alex lexer, but it's simple enough
-- to implement by hand.
convChar :: String -> String
convChar ('\\':'u':s@(d1:d2:d3:d4:s')) =
  -- TODO: this is the wrong place for handling unicode escapes
  -- according to the Java Language Specification. Unicode escapes can
  -- appear anywhere in the source text, and are best processed
  -- before lexing.
  if all isHexDigit [d1,d2,d3,d4]
  then toEnum (read ['0','x',d1,d2,d3,d4]):convChar s'
  else lexicalError $ "bad unicode escape \"\\u" ++ take 4 s ++ "\""
convChar ('\\':'u':s) =
  lexicalError $ "bad unicode escape \"\\u" ++ take 4 s ++ "\""
convChar ('\\':c:s) =
  if isOctDigit c
  then convOctal maxRemainingOctals
  else (case c of
          'b' -> '\b'
          'f' -> '\f'
          'n' -> '\n'
          'r' -> '\r'
          't' -> '\t'
          '\'' -> '\''
          '\\' -> '\\'
          '"' -> '"'
          _ -> badEscape):convChar s
  where maxRemainingOctals =
          if c <= '3' then 2 else 1
        convOctal n =
          let octals = takeWhile isOctDigit $ take n s
              noctals = length octals
              toChar = toEnum . fst . head . readOct
          in toChar (c:octals):convChar (drop noctals s)
        badEscape = lexicalError $ "bad escape \"\\" ++ c:"\""
convChar ("\\") =
  lexicalError "bad escape \"\\\""
convChar (x:s) = x:convChar s
convChar "" = ""

lexError :: String -> Alex a
lexError s = do
  (p,c,_,input) <- alexGetInput
  alexError (showPosn p ++ ": " ++ s ++ 
                   (if (not (null input))
                     then " before " ++ show (head input)
                     else " at end of file"))

showPosn :: AlexPosn -> String
showPosn (AlexPn _ line col) = show line ++ ':': show col

alexEOF :: Alex Token
alexEOF = do
  (p,_,_,_) <- alexGetInput
  return $ T p TEOF ""

lexer :: (Token -> Alex a) -> Alex a
lexer = (alexMonadScan >>=)
}
