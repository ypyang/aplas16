{
module Language.Fun.Parser.Parser (
  parseString,
  ) where

import Language.Fun.Parser.Lexer
import Language.Fun.Src
import Language.Fun.Lit
}

%name parse
%tokentype { Token }
%monad { Alex } { (>>=) } { return }
%lexer { lexer } { T _ TEOF "" }
%error { parseError }

%token

'()' {T _ TSym "()" }

';' { T _ TSym ";" }
',' { T _ TSym "," }
'(' { T _ TSym "(" }
')' { T _ TSym ")" }
'{' { T _ TSym "{" }
'}' { T _ TSym "}" }
'*' { T _ TSym "*" }
'_' { T _ TSym "_" }

mul { T _ TSym "**" }
'++' { T _ TSym "++" }
'->' { T _ TSym "->" }
'=>' { T _ TSym "=>" }
conlam { T _ TSym "\\~" }
'==' { T _ TSym "==" }
'/=' { T _ TSym "/=" }
'>=' { T _ TSym ">=" }
'<=' { T _ TSym "<=" }
'&&' { T _ TSym "&&" }
'||' { T _ TSym "||" }

':' { T _ TSym ":" }
'::' { T _ TSym "::" }
'=' { T _ TSym "=" }
'$' { T _ TSym "$" }
'~' { T _ TSym "~" }
'|' { T _ TSym "|" }
'.' { T _ TSym "." }
'^' { T _ TSym "^" }
'+' { T _ TSym "+" }
'-' { T _ TSym "-" }
'<' { T _ TSym "<" }
'>' { T _ TSym ">" }
'!' { T _ TSym "!" }
'#' { T _ TSym "#" }
show { T _ TSym "@" }
lam { T _ TSym "\\" }
mu { T _ TSym "/" }

data { T _ TKey "data" }
def { T _ TKey "def" }
defrec { T _ TKey "defrec" }
type { T _ TKey "type" }
let { T _ TKey "let" }
letrec { T _ TKey "letrec" }
in { T _ TKey "in" }
case { T _ TKey "case" }
of { T _ TKey "of" }
castupw { T _ TKey "castup" }
castdnw { T _ TKey "castdn" }
castupf { T _ TKey "castup!" }
castdnf { T _ TKey "castdn!" }
intty { T _ TKey "Int" }
strty { T _ TKey "String" }
boolty { T _ TKey "Bool" }
true { T _ TKey "True" }
false { T _ TKey "False" }
if { T _ TKey "if" }
then { T _ TKey "then" }
else { T _ TKey "else" }
import { T _ TKey "import" }
module { T _ TKey "module" }
as { T _ TKey "as" }
explicit { T _ TKey "explicit" }

num { T _ (TInt _) _ }
str { T _ (TStr _) _ }
id { T _ (TVar _) _ }
op { T _ (TOpSym _) _ }

%right in
%right ONEALT
%right '|'
%nonassoc '::'
%right '.'
%right '=>'
%right '->'
%right '$'
%right then else
%nonassoc '>' '<' '>=' '<=' '==' '/='
%left '||'
%left '&&'
%left op
%left '+' '-' '++'
%left mul
%left CASTDN NEG SHOW '!' CONAPP

%%

prog : moddecl decllst expr_or_unit { Prog ($1 ++ $2) $3 }

modname : id { splitModName (unId $1) }

moddecl : {- empty -} { [] }
        | module modname exportlst ';' { [Export (mkL $1) $2 $3] }

exportlst : {- empty -} { [] }
          | '(' modvars ')' { $2 }

modvars : id { [newVar (unId $1)] }
        | id ',' modvars { (newVar (unId $1)) : $3 }

imp : import { Import (mkL $1) False }
    | import explicit { Import (mkL $1) True }

expr_or_unit : expr { $1 }
             | {- empty -} { Unit NoLoc }

decllst : {- empty -} { [] }
        | decl ';' decllst { $1 : $3 }

decl : def opid defann '=' expr
       { if null $3 then VarDefInf (mkL $1) (newVar $2) $5 else VarDef (mkL $1) (newVar $2) (head $3) $5 }
     | def opid lamteles defann '=' expr
       { if null $4 then VarDefInf (mkL $1) (newVar $2) (teleToBind (LamAnn NoLoc) $6 $3)
         else VarDef (mkL $1) (newVar $2) (teleToBind (Pi NoLoc) (head $4) $3) (teleToIBind (Lam NoLoc) $6 $3) }
     | type opid teleidlst '=' expr
       { VarDef (mkL $1) (newVar $2) (teleToBind (Pi NoLoc) (Star NoLoc) $3) (teleToIBind (Lam NoLoc) $5 $3) }
     | defrec opid ':' expr '=' expr { VarRec (mkL $1) (newVar $2) $4 $6 }
     | defrec opid lamteles ':' expr '=' expr
       { VarRec (mkL $1) (newVar $2) (teleToBind (Pi NoLoc) $5 $3) (teleToIBind (Lam NoLoc) $7 $3) }
     | data opid teleargs { Data (mkL $1) (newVar $2) (bindAll $3 []) }
     | data opid teleargs '=' sdatabnds { DataSimp (mkL $1) (newVar $2) (bindAll $3 $5) }
     | data opid teleargs '=' id '{' recbnds '}' { Rec (mkL $1) (newVar $2) (bindAll $3 (newVar (unId $5), $7)) }
     | imp modname as modname exportlst { $1 $2 (Just $4) $5 }
     | imp modname exportlst { $1 $2 Nothing $3 }

opid : id { unId $1 }
     | op { "Op(" ++ unOp $1 ++ ")" }

defann : {- empty -} { [] }
       | ':' expr { [$2] }

tele : '(' id ':' expr ')' { (newVar (unId $2), $4) }

coneq : expr '~' expr { Eq $1 $3 }

teleid : tele { $1 }
       | id { (newVar (unId $1), Star NoLoc) }

teleidlst : {- empty -} { [] }
          | teleid teleidlst { $1 : $2 }

teleargs : {- empty -} { EmptyArgs }
         | teleid teleargs { ConsArgs (teleToRebind $1 $2) }

lamtele : '(' id ':' expr ')' { (newVar (unId $2), $4) }
        | '(' '_' ':' expr ')' { (dummyvar, $4) }

lamteles : lamtele { [$1] }
         | lamtele lamteles { $1 : $2 }

coneqs : coneq { [$1] }
       | coneq ',' coneqs { $1 : $3 }

recbnds : databnd { [$1] }
        | databnd ',' recbnds { $1 : $3 }

sdatabnds : sdatabnd { [$1] }
          | sdatabnd '|' sdatabnds { $1 : $3 }

databnd : id ':' expr { (newVar (unId $1), $3) }

sdatabnd : id exprlst { (newVar (unId $1), $2) }

exprlst : {- empty -} { [] }
        | atom exprlst { (dummyvar, $1) : $2 }
        | tele exprlst { $1 : $2 }

expr : lam id ':' expr '.' expr { LamAnn (mkL $1) (newBind (unId $2) $4 $6) }
     | lam '_' ':' expr '.' expr { LamAnn (mkL $1) (vnbind dummyvar $4 $6) }
     | lam lamteles '.' expr { teleToBind (LamAnn (mkL $1)) $4 $2 }
     | lam idlst '.' expr { idToIBind (Lam (mkL $1)) $4 $2 }
     | expr '->' expr { Pi (getLoc $1) (vnbind dummyvar $1 $3) }
     | '(' id ':' expr ')' '->' expr { Pi (mkL $1) (newBind (unId $2) $4 $7) }
     | mu id '.' expr { Mu (mkL $1) (newIBind (unId $2) $4) }
     | expr '$' expr { App (getLoc $1) $1 $3 }
     | case expr of alts { Case (mkL $1) $2 $4 }
     | let id defann '=' expr in expr
       { Let (mkL $1) (newBind (unId $2) (if null $3 then $5 else (Anno NoLoc $5 (head $3))) $7) }
     | let id lamteles defann '=' expr in expr
       { Let (mkL $1) (newBind (unId $2) (teleToBind (LamAnn NoLoc)
              (if null $4 then $6 else (Anno NoLoc $6 (head $4))) $3) $8) }
     | letrec id ':' expr '=' expr in expr { LetRec (mkL $1) (newBind (unId $2) (Anno NoLoc $6 $4) $8) }
     | letrec id lamteles ':' expr '=' expr in expr 
       { LetRec (mkL $1) (newBind (unId $2) (Anno NoLoc (teleToBind (LamAnn NoLoc) $7 $3) (teleToBind (Pi NoLoc) $5 $3)) $9) }
     | if expr then expr else expr { IfThenElse (mkL $1) $2 $4 $6 }

     | cast expr %prec CASTDN { $1 1 $2 }
     | cast caststep expr %prec CASTDN { $1 $2 $3 }
     | expr '::' expr { Anno (getLoc $1) $1 $3 }

     | expr '+' expr { LBin (getLoc $1) LOpAdd $1 $3 }
     | expr '-' expr { LBin (getLoc $1) LOpMinus $1 $3 }
     | expr mul expr { LBin (getLoc $1) LOpMul $1 $3 }
     | '-' expr %prec NEG { LUnary (mkL $1) LOpNeg $2 }
     | expr '++' expr { LBin (getLoc $1) LOpConcat $1 $3 }
     | show expr %prec SHOW { LUnary (mkL $1) LOpShow $2 }
     | expr '<' expr { LBin (getLoc $1) LOpL $1 $3 }
     | expr '>' expr { LBin (getLoc $1) LOpG $1 $3 }
     | expr '<=' expr { LBin (getLoc $1) LOpLE $1 $3 }
     | expr '>=' expr { LBin (getLoc $1) LOpGE $1 $3 }
     | expr '==' expr { LBin (getLoc $1) LOpEq $1 $3 }
     | expr '/=' expr { LBin (getLoc $1) LOpNeq $1 $3 }
     | expr '&&' expr { LBin (getLoc $1) LOpAnd $1 $3 }
     | expr '||' expr { LBin (getLoc $1) LOpOr $1 $3 }
     | '!' expr { LUnary (mkL $1) LOpNot $2 }

     | expr op expr { App (getLoc $1) (App (getLoc $3) (Var (mkL $2) . newVar $ "Op(" ++ unOp $2 ++ ")") $1) $3 }
     | fact { $1 }

fact : fact atom { App (getLoc $1) $1 $2 }
     | atom { $1 }

atom : num { LVal (mkL $1) (LVInt (unNum $1)) }
     | str { LVal (mkL $1) (LVStr (unStr $1)) }
     | true { LVal (mkL $1) (LVBool True) }
     | false { LVal (mkL $1) (LVBool False) }
     | id { Var (mkL $1) (newVar (unId $1)) }
     | intty { LTy (mkL $1) LTInt }
     | strty { LTy (mkL $1) LTStr }
     | boolty { LTy (mkL $1) LTBool }
     | '*' { Star (mkL $1) }
     | '_' { Any (mkL $1) }
     | '()' { Unit (mkL $1) }
     | '(' expr ')' { $2 }

alts : alt %prec ONEALT { [$1] }
     | alt '|' alts { $1 : $3 }

alt : id idlst '=>' expr { (newVar (unId $1), bindAll $2 $4) }
    | '_' '=>' expr { (dummyvar, bindAll [] $3) }

idlst : {- empty -} { [] }
      | id idlst { newVar (unId $1) : $2 }
      | '_' idlst { dummyvar : $2 }

nstep : '^' num { unNum $2 }
      | '^' '(' '-' num ')' { -(unNum $4) }

cast : castupw { Cast (mkL $1) Up Weak }
     | castupf { Cast (mkL $1) Up Full }
     | castdnw { Cast (mkL $1) Down Weak }
     | castdnf { Cast (mkL $1) Down Full }

caststep : nstep { $1 }

{
splitModName :: String -> [String]
splitModName = foldr f [[]]
  where f c l@(x:xs) | c == '.' = []:l
                     | otherwise = (c:x):xs

parseError :: Token -> Alex a
parseError (T p _ s) =
  alexError (showPosn p ++ ": parse error at token '" ++ s ++ "'")

parseString :: String -> Either String Prog
parseString inp = runAlex inp parse

mkL :: Token -> Loc
mkL (T (AlexPn _ row col) _ _) = Located row col

unNum :: Token -> Int
unNum (T _ (TInt x) _) = x

unStr :: Token -> String
unStr (T _ (TStr x) _) = x

unId :: Token -> String
unId (T _ (TVar x) _) = x

unOp :: Token -> String
unOp (T _ (TOpSym x) _) = x
}
