module Language.Fun.Trans (
  runM,
  transProg,
  M,
  Ctx,
  emptyCtx,
  ctxMerge,
  Cache,
  emptyCache,
  cacheMerge,
  ) where

import           Control.Monad.Except
import           Control.Monad.Identity
import           Control.Monad.State
import           Control.Monad.Writer             hiding (All, Any, (<>))
import qualified Data.Hashable                    as H
import qualified Data.LinkedHashMap.IntMap        as HM
import           Data.List                        (find)
import qualified Data.Map.Strict                  as M
import qualified Language.Fun.Core                as C
import           Language.Fun.Lit
import           Language.Fun.Pretty
import           Language.Fun.Src                 hiding (M, runM)
import qualified Language.Fun.Src                 as S (M)
import qualified Language.Fun.Typecheck           as T
import           Prelude                          hiding ((<$>), (<*>))
import           Text.PrettyPrint.ANSI.Leijen     hiding (Pretty)
import           Unbound.Generics.LocallyNameless hiding (subst)

type M = ExceptT Doc (WriterT [Doc] (StateT Cache (FreshMT Identity)))

convCM :: C.M a -> M a
convCM = lift . lift . lift

convSM :: S.M a -> M a
convSM = lift . lift . lift

convTM :: Loc -> T.M a -> M a
convTM l m = let (ret, trs) = T.runM m in
  do traces trs
     case ret of
       Left msg -> throwError $ pp l <> msg
       Right x -> return x

type Cache = M.Map VarName DataCache
data DataCache = DataCache ([Tele], [C.Tele]) [(C.VarName, VarName, Int)]
  (M.Map VarName ([Tele], [C.Tele], [C.Tm], C.VarName))

emptyCache :: Cache
emptyCache = M.empty

cachePut :: Cache -> M ()
cachePut = lift . put

cacheGet :: M Cache
cacheGet = lift get

cacheAdd :: VarName -> DataCache -> M ()
cacheAdd name dc = do
  c <- cacheGet
  cachePut $ M.insert name dc c

cacheMerge :: Cache -> Cache -> Cache
cacheMerge = flip M.union

mapFind :: Ord k => k -> M.Map k v -> Doc -> M v
mapFind key m d = case M.lookup key m of
  Just v -> return v
  _ -> throwError d

hmapFind :: (H.Hashable k, Eq k) => k -> HM.LinkedHashMap k v -> Doc -> M v
hmapFind key m d = case HM.lookup key m of
  Just v -> return v
  _ -> throwError d

type VarCtx = HM.LinkedHashMap VarName (Ty, C.VarName)
type ConCtx = T.ConCtx
type BndCtx = T.BndCtx
type SBndCtx = HM.LinkedHashMap VarName Tm

data Ctx = Ctx {
  varCtx :: VarCtx,
  conCtx :: T.ConCtx,
  bndCtx :: T.BndCtx,
  sbndCtx  :: SBndCtx
  } deriving (Show)

ppMapCtx :: String -> VarCtx -> Doc
ppMapCtx s = strsep ", " . map (\(v, (t, _)) -> vardoc v <+>
                                           text s <+>
                                           pp t) . HM.toList

ppVarCtx :: VarCtx -> [Doc]
ppVarCtx c = if HM.null c then [] else [ppMapCtx ":" c]

instance Pretty Ctx where
  ppr _ ctx = strsep "; " $ concat
    [ppVarCtx (varCtx ctx), T.ppConCtx (conCtx ctx)]

runM :: Cache -> M a -> ((Either Doc a, [Doc]), Cache)
runM cache = runIdentity . runFreshMT . (`runStateT` cache) . runWriterT . runExceptT

trace :: Doc -> M ()
trace s = lift (tell [s])

traces :: [Doc] -> M ()
traces s = lift (tell s)

emptyCtx :: Ctx
emptyCtx = Ctx {
  varCtx = HM.empty,
  conCtx = [],
  bndCtx = HM.empty,
  sbndCtx = HM.empty
  }

ctxMerge :: Ctx -> Ctx -> Ctx
ctxMerge x y = Ctx {
  varCtx = foldl (\c (k, v) -> HM.insert k v c) (varCtx x) (HM.toList $ varCtx y),
  conCtx = conCtx x ++ conCtx y,
  bndCtx = foldl (\c (k, v) -> HM.insert k v c) (bndCtx x) (HM.toList $ bndCtx y),
  sbndCtx = foldl (\c (k, v) -> HM.insert k v c) (sbndCtx x) (HM.toList $ sbndCtx y)
  }

ctxMap :: (VarCtx -> VarCtx) ->
          (ConCtx -> ConCtx) ->
          (BndCtx -> BndCtx) ->
          (SBndCtx -> SBndCtx) ->
          Ctx -> Ctx
ctxMap f1 f2 f3 f4 ctx = Ctx {
  varCtx = f1 (varCtx ctx),
  conCtx = f2 (conCtx ctx),
  bndCtx = f3 (bndCtx ctx),
  sbndCtx = f4 (sbndCtx ctx)
  }

ctxAddVar :: Ctx -> VarName -> C.VarName -> Ty -> Ctx
ctxAddVar ctx v cv t = ctxMap (if isDummyVar v then id else HM.insert v (t, cv)) id id id ctx

ctxAddVars :: Ctx -> [Tele] -> [C.VarName] -> Ctx
ctxAddVars ctx teles cnames = foldl (\c ((v, t), cv) -> ctxAddVar c v cv t) ctx (zip teles cnames)

ctxAddCons :: Ctx -> [C.Con] -> Ctx
ctxAddCons ctx cs = ctxMap id (++ cs) id id ctx

ctxAddBnd :: Ctx -> C.VarName -> C.Tm -> Ctx
ctxAddBnd ctx v t = ctxMap id id (if T.isValidVar v then HM.insert v t else id) id ctx

isValidVar :: VarName -> Bool
isValidVar = not . isDummyVar

ctxAddSBnd :: Ctx -> VarName -> Tm -> Ctx
ctxAddSBnd ctx v t = ctxMap id id id (if isValidVar v then HM.insert v t else id) ctx

ctxAddBnds :: Ctx -> [C.Tele] -> Ctx
ctxAddBnds = foldl (\c (v, t) -> ctxAddBnd c v t)

ctxFindVar :: Loc -> Ctx -> VarName -> M (Tm, C.VarName)
ctxFindVar l ctx v = hmapFind v (varCtx ctx)
  (pp l <> text "variable" <+>
   squotes (vardoc v) <+>
   text "not in the scope")

bndsSubst :: [Tele] -> Tm -> Tm
bndsSubst bnds tm =
  let mkSubs [] = []
      mkSubs (a@(x, e):as) = a : mkSubs (map (\(x', e') -> (x', vnSubst x e e')) as)
  in foldl (\v (x, e) -> vnSubst x e v) tm (mkSubs bnds)

ctxSubst :: Ctx -> Tm -> Tm
ctxSubst c = bndsSubst (HM.toList . sbndCtx $ c)

conCheck_ :: Ctx -> Con -> M Bool
conCheck_ ctx con = do
  let (Eq ty ty') = substCtxCon ctx con
  return $ ty `aeq` ty'
  where
    substCtxCon c (Eq ty1 ty2) = Eq (ctxSubst c ty1) (ctxSubst c ty2)

conCheck :: Ctx -> Con -> Doc -> M ()
conCheck ctx con d = do
  ret <- conCheck_ ctx con
  unless ret $ throwError d

transCtx :: Ctx -> T.Ctx
transCtx ctx = T.Ctx { T.varCtx = HM.empty,
                       T.conCtx = conCtx ctx,
                       T.bndCtx = bndCtx ctx}

transProg :: Ctx -> Prog -> M (Ctx, Ty, T.Ctx, C.Tm)
transProg ctx (Prog ds tm) = do
  (ctx', corectx) <- transDecls ctx ds
  let ctxRet = ctxMerge ctx ctx'
  (ty, coretm) <- transTm ctxRet tm
  return (ctxRet, ty, corectx, coretm)

transDecls :: Ctx -> [Decl] -> M (Ctx, T.Ctx)
transDecls _ [] = return (emptyCtx, T.emptyCtx)
transDecls ctx (d:ds) = do
  (ctx', corectx) <- transDecl ctx d
  (ctx'', corectx') <- transDecls (ctxMerge ctx ctx') ds
  return (ctxMerge ctx' ctx'', T.ctxMerge corectx corectx')

transDecl :: Ctx -> Decl -> M (Ctx, T.Ctx)
transDecl ctx inp = do
  trace $ pp ctx <+> text "|=>" <+> pp inp
  let raise :: Loc -> String -> M a
      raise l t = throwError (pp l <> text t)
  case inp of
    VarDefInf _ v tm -> do
      let v' = transVar v
      (ty, tm') <- transTm ctx tm
      ty' <- kindCheck ctx ty
      let rctx = ctxAddVar (ctxAddBnd (ctxAddSBnd emptyCtx v tm) v' tm') v v' ty
          rctx' = T.ctxAddVar (T.ctxAddBnd T.emptyCtx v' tm') v' ty'
      return (rctx, rctx')

    Data _ d databinds -> do
      (args, bd) <- unbind databinds
      let as = dataArgsToTele args
          datasig = teleToBind (Pi NoLoc) (Star NoLoc) as
          ctorbinds = map (\(ctor, sig) ->
                            (ctor,
                             teleToBind (Pi NoLoc) sig as)) bd
          ctorsigs = map snd bd
          ctornames = map fst bd
          d' = transVar d
          ctornames' = map transVar ctornames
          varctx = ctxAddVars (ctxAddVar emptyCtx d d' datasig) ctorbinds ctornames'
      as_star' <- kindCheck ctx datasig
      as' <- do
        (as', star) <- convCM $ C.clusterPi as_star'
        case star of
          C.Star -> return as'
          _ -> raise NoLoc "failsafe: not star"
      let ctx_as_d = ctxAddVars (ctxAddVar ctx d d' datasig) as (map fst as')
      bs_es <- convSM $ mapM clusterPi ctorsigs
      let ctorarglens = map (length . fst) bs_es
      bses_bsjesj_retTys' <- forM ctorsigs $ \ctorsig -> do
        let renameClusterPi = viewVars
              where viewVars (C.Pi b) = do
                      ((n, Embed t), e) <- unbind b
                      let n' = if C.isDummyVar n then n else C.tmpVar n
                      (vars, body) <- viewVars (C.vnSubst n (C.Var n') e)
                      return ((n', t) : vars, body)
                    viewVars e = return ([], e)
        ctorsig' <- kindCheck ctx_as_d ctorsig
        (bs_raw', ret') <- convCM $ C.clusterPi ctorsig'
        (bsj_raw', retj') <- renameClusterPi ctorsig'
        let namelist = C.tmpVarList "b"
            bs' = zipWith (\t@(v, e) n ->
                            if C.isDummyVar v then
                              (n, e) else t) bs_raw' namelist
            namelist' = C.tmpVarList (tmpName "b")
            bsj' = zipWith (\t@(v, e) n ->
                             if C.isDummyVar v then
                               (n, e) else t) bsj_raw' namelist'
        case C.flattenApp ret' of
          [] -> raise NoLoc "failsafe: unexpceted flatten app"
          (dvar':es') -> case dvar' of
            C.Var v -> if v == d' then return ((bs', es'), (bsj', tail (C.flattenApp retj')), ret')
                       else raise NoLoc "failsafe: return datatype not matched"
            _ -> raise NoLoc "failsafe: return is not datatype"
      let bs_es' = map (\(x,_,_) -> x) bses_bsjesj_retTys'
          bsj_esj' = map (\(_,x,_) -> x) bses_bsjesj_retTys'
          retTys' = map (\(_,_,x) -> x) bses_bsjesj_retTys'
      let avars' = map fst as'
          aVarTms' = map C.Var avars'
          fvar' = C.tmpVar d'
          fvarTm' = C.Var fvar'
          ctorty' (bs', es') tm' = C.Pi $ C.vnbind C.dummyvar
            (C.teleToBind C.Pi
             (C.mkConPi (zipWith C.Eq aVarTms' es') fvarTm') bs') tm'
          ctortys' = foldr ctorty' fvarTm' bs_es'
          dCore = C.Mu (C.vnbind d' as_star'
                        (C.teleToBind C.Lam
                         (C.Pi (C.vnbind fvar' C.Star ctortys')) as'))
          cvars' = map C.tmpVar ctornames'
          ctor_cvar_bs_es' = zipWith (\(ctor', cvar') (bs', es') ->
                                       (ctor', cvar', bs', es')) (zip ctornames' cvars') bs_es'
          cvar_bsj_esj' = zipWith (\cvar' (bs', es') ->
                                  (cvar', bs', es')) cvars' bsj_esj'
          mkcvar' esi' (cvar', bsj', esj') tm' =
            let subs = zip avars' esi'
                bsj'' = map (\(v, t) -> (v, C.substVec t subs)) bsj'
                esj'' = map (`C.substVec` subs) esj'
                cons = zipWith C.Eq esi' esj''
            in C.Lam (C.vnbind cvar'
                      (C.teleToBind C.Pi
                       (C.mkConPi cons fvarTm') bsj'') tm')
          mkcvars' tm' esi' = foldr (mkcvar' esi') tm' cvar_bsj_esj'
          mkCtorCore (ctor', cvar', bs', es') retTy' =
            let nstep = length as'
                cvarTm' = C.Var cvar'
                bvarTms' = map (C.Var . fst) bs'
            in (ctor', C.teleToBind C.Lam
                (C.teleToBind C.Lam
                 (C.Cast C.Up C.Weak (nstep + 1) (Just retTy')
                  (C.Lam (C.vnbind fvar' C.Star
                          (mkcvars' (C.mkConApp nstep
                                     (foldl C.App cvarTm' bvarTms'))
                           es')))) bs') as')
          varctx' = T.ctxAddVars
            T.emptyCtx ((d', as_star'):
                        zipWith (\(ctor', _, bs', _) retTy' ->
                                  (ctor',
                                   C.teleToBind C.Pi retTy'
                                   (as' ++ bs'))) ctor_cvar_bs_es' retTys')
          retbnds' = (d', dCore) : zipWith mkCtorCore ctor_cvar_bs_es' retTys'

      let retctx' =  T.ctxAddBnds varctx' retbnds'
          retctx = ctxAddBnds varctx retbnds'
          cache = DataCache (as, as') (zip3 ctornames' ctornames ctorarglens)
            (foldl (\v (((ctor, (bs, _)), (bs', es')), ctor') ->
                     M.insert ctor (bs, bs', es', ctor') v)
             M.empty (zip (zip (zip ctornames bs_es) bs_es') ctornames'))

      cacheAdd d cache
      return (retctx, retctx')

    DataSimp l _ _ -> raise l "unexpected simple datatype, not desugared?"
    Rec l _ _ -> raise l "unexpected record, not desugared?"
    VarRec l _ _ _ -> raise l "unexpected recursive define, not desugared?"

    _ -> raise (getLocDecl inp) "unexpected declaration"

kindCheck :: Ctx -> Tm -> M C.Tm
kindCheck c tm = case tm of
  Star _ -> return C.Star
  Any _ -> return C.Any
  Unit _ -> return C.Unit
  _ -> checkTm c tm (Star NoLoc)

litOpCheck :: Loc -> (LitTy, LitTy) -> (Ty, C.Ty) -> Ctx -> (Tm, Tm) -> M LitTy
litOpCheck l (inTy, outTy) (ty, _) ctx (inp, e) =
  case inTy of
    LTAny -> return outTy
    _ -> case ty of
      LTy _ lty -> if lty /= inTy then throwError (pp l <> text "expect" <+> squotes (pp inTy) <+>
                                                   text "but got" <+> squotes (pp ty))
                 else return outTy
      _ -> do
        conCheck ctx (Eq ty (LTy NoLoc inTy))
          (hang 2 $ pp l <> text "literal operation type mismatch in" <+>
           squotes (pp inp) <> colon <$>
           text "expression" <+> squotes (pp e) <+>
           text "has type" <+> squotes (pp ty) <$>
           text "but requires type" <+> squotes (pp inTy))
        return outTy

transVar :: VarName -> C.VarName
transVar = string2Name . name2String

transCastAnn :: CastAnn -> C.CastAnn
transCastAnn x = case x of
  Weak -> C.Weak
  Full -> C.Full

-- transCon :: Ctx -> Con -> M C.Con
-- transCon ctx (Eq ty1 ty2) = C.Eq `fmap` transTm_ ctx ty1 <*> transTm_ ctx ty2
--   where transTm_ c = liftM snd . transTm c

-- transCons :: Ctx -> [Con] -> M [C.Con]
-- transCons ctx = mapM (transCon ctx)

checkTm :: Ctx -> Tm -> Ty -> M C.Tm
checkTm ctx inp inpty =  do
  trace $ pp ctx <+> text "|~<=" <+> pp inp
    <+> text "::" <+> pp inpty
  let raise :: Loc -> String -> M a
      raise l t = throwError (pp l <> text t)
  case inp of
    Lam l b -> do
      piTy' <- kindCheck ctx inpty
      t1' <- case piTy' of
        C.Pi b' -> do
          ((_, Embed t1'), _) <- unbind b'
          return t1'
        _ -> raise l "not a pi type"
      (x, e) <- unbind b
      (t1, t2) <- case inpty of
        Pi _ b' -> do
          ((x', Embed t1), t2) <- unbind b'
          return (t1, vnSubst x' (Var NoLoc x) t2)
        _ -> raise l "not a pi type"
      e' <- checkTm (ctxAddVar ctx x (transVar x) t1) e t2
      return (C.Lam (C.vnbind (transVar x) t1' e'))
    LamAnn l b -> do
      piTy' <- kindCheck ctx inpty
      t1' <- case piTy' of
        C.Pi b' -> do
          ((_, Embed t1'), _) <- unbind b'
          return t1'
        _ -> raise l "not a pi type"
      ((x, Embed t1), e) <- unbind b
      (tyt1, t2) <- case inpty of
        Pi _ b' -> do
          ((x', Embed tyt1), t2) <- unbind b'
          return (tyt1, vnSubst x' (Var NoLoc x) t2)
        _ -> raise NoLoc "failsafe: not a pi type"
      unless (aeq t1 tyt1) $ throwError
        (hang 2 $ pp l <> text "annoated lambda type mismatch" <+>
         squotes (pp inp) <+> text "has annotation" <+> squotes (pp t1) <$>
         squotes (pp inpty) <+> text "has annotation" <+> squotes (pp tyt1))
      e' <- checkTm (ctxAddVar ctx x (transVar x) t1) e t2
      return (C.Lam (C.vnbind (transVar x) t1' e'))
    Mu _ b -> do
      (x, e) <- unbind b
      e' <- checkTm (ctxAddVar ctx x (transVar x) inpty) e inpty
      t1' <- kindCheck ctx inpty
      return (C.Mu (C.vnbind (transVar x) t1' e'))
    Cast l ud ann step e -> case inpty of
      Any{} -> raise l "missing type annotation in cast"
      t1 -> do
        (t2, e') <- transTm ctx e
        t2' <- kindCheck ctx t2
        t1' <- kindCheck ctx t1
        let ctx' = transCtx ctx
            ann' = transCastAnn ann
        ud' <- case ud of
          Up -> convTM l (T.reduceCheck ann' ctx' step t1' t2' (pp inp)) >> return C.Up
          Down -> convTM l (T.reduceCheck ann' ctx' step t2' t1' (pp inp)) >> return C.Down
        return (C.Cast ud' ann' step (Just t1') e')
    ConLam{} -> raise NoLoc "fatal: conlam"
    _ -> do
      (t1, e') <- transTm ctx inp
      conCheck ctx (Eq t1 inpty)
        (hang 2 $ pp (getLoc inp) <> text "type mismatch in" <+>
         squotes (pp inp) <> colon <$>
         text "actual type " <+> squotes (pp t1) <$>
         text "but requires" <+> squotes (pp inpty))
      return e'

transTm :: Ctx -> Tm -> M (Ty, C.Tm)
transTm ctx inp = do
  trace $ pp ctx <+> text "|~=>" <+> pp inp
  let raise :: Loc -> String -> M a
      raise l t = throwError (pp l <> text t)
  case inp of
    Anno _ e t -> do
      e' <- checkTm ctx e t
      return (t, e')
    Var l v -> do
      (ty, v') <- ctxFindVar l ctx v
      return (ty, C.Var v')
    Star{} -> return (Star NoLoc, C.Star)
    App l e1 e2 -> do
      (piTy1, e1') <- transTm ctx e1
      (x, ty1, ty2) <- case piTy1 of
        Pi _ b -> do
          ((x, Embed ty1), ty2) <- unbind b
          return (x, ty1, ty2)
        _ -> throwError $ pp l <>
          text "expect a pi type for" <+> squotes (pp e1) <+>
          text "but got" <+> squotes (pp piTy1)
      e2' <- checkTm ctx e2 ty1
      return (vnSubst x e2 ty2, C.App e1' e2')
    LamAnn _ b -> do
      ((x, Embed t1), e) <- unbind b
      (t2, e') <- transTm (ctxAddVar ctx x (transVar x) t1) e
      let piTy = Pi NoLoc (vnbind x t1 t2)
      piTy' <- kindCheck ctx piTy
      t1' <- case piTy' of
        C.Pi b' -> do
          ((_, Embed t1'), _) <- unbind b'
          return t1'
        _ -> raise NoLoc "failsafe: not a pi type"
      return (piTy, C.Lam (C.vnbind (transVar x) t1' e'))
    Pi _ b -> do
      ((x, Embed t1), t2) <- unbind b
      t1' <- kindCheck ctx t1
      t2' <- kindCheck (ctxAddVar ctx x (transVar x) t1) t2
      return (Star NoLoc, C.Pi (C.vnbind (transVar x) t1' t2'))
    Case l p alts -> do
      (pTy, p') <- transTm ctx p
      (dname, ws, ws') <- do
        let d_ws = flattenApp pTy
        case d_ws of
          [] -> raise NoLoc "failsafe: empty datatype"
          (d:ws) -> case d of
            Var l dvar -> do
              (dty, dvar') <- ctxFindVar l ctx dvar
              pTy' <- kindCheck (ctxAddVar ctx dvar dvar' dty) pTy
              let d_ws' = C.flattenApp pTy'
              if null d_ws' then raise NoLoc "failsafe: empty trans. datatype"
                else return (dvar, ws, tail d_ws')
            _ -> throwError $ pp l <>
              text "scrutinee" <+>
              squotes (pp p) <+>
              text "is not a datatype"
      (as, as', ctornames', ctormap) <- do
        cache <- cacheGet
        datacore <- mapFind dname cache (pp l <> text "datatype" <+>
                                         squotes (vardoc dname) <+>
                                         text "not defined")
        let DataCache (as, as') ctornames' ctormap = datacore
        return (as, as', ctornames', ctormap)
      let transAlt (k, bndmany) = if isDummyVar k then return (C.dummyvar, (dummyvar, Any NoLoc, C.Any, C.Any)) else do
            (xs, q) <- unbind bndmany
            (bs, bs', es', k') <- mapFind k ctormap
              (pp l <> text "constructor" <+>
               squotes (vardoc k) <+>
               text "not defined")
            when (length bs /= length xs) (throwError $ pp l <>
                                          text "constructor" <+>
                                          squotes (vardoc k) <+>
                                          text "applied wrong number of args")
            let subs_aw = zip (map fst as) ws
                subs_bx = zip (map fst bs) (map (Var NoLoc) xs)
                map_aw_bx e = substVec (substVec e subs_aw) subs_bx
                x_t_subed = zipWith (\x (_, t) -> (x, map_aw_bx t)) xs bs
            let xs' = map transVar xs
                subs_aw' = zip (map fst as') ws'
                subs_bx' = zip (map fst bs') (map C.Var xs')
                map_aw_bx' e' = C.substVec (C.substVec e' subs_aw') subs_bx'
                x_t_subed' = zipWith (\x' (_, t') -> (x', map_aw_bx' t')) xs' bs'
                w_e_cons' = zipWith (\w' e' -> C.Eq w' (map_aw_bx' e')) ws' es'
            let ctxWithArgs = ctxAddCons (ctxAddVars ctx x_t_subed xs') w_e_cons'
            (qty, q') <- transTm ctxWithArgs q
            qty' <- kindCheck ctxWithArgs qty
            return (k', (k, qty, qty', C.teleToBind C.Lam (C.mkConLam w_e_cons' q') x_t_subed'))
      kqq_bodys' <- do
        defaultRet <- case find (\(v, _) -> isDummyVar v) alts of
          Just (_, bndmany) -> do (_, q) <- unbind bndmany; return q
          _ -> return (Any NoLoc)
        k_alts' <- mapM transAlt alts
        let altsMap' = M.fromList k_alts'
        forM ctornames' $ \(ctor', ctor, len) ->
          case M.lookup ctor' altsMap' of
            Just e' -> return e'
            _ -> snd `fmap` transAlt (ctor, bind (take len (tmpVarList "x")) defaultRet)
      (r2, r', bodys') <- do
        when (null kqq_bodys') $ raise l "case has empty branches"
        let findNotAny [] = head kqq_bodys'
            findNotAny (x@(_, rr, _, _):xs) =
              case rr of
                Any{} -> findNotAny xs
                _ -> x
            (k, r, r', _)  = findNotAny kqq_bodys'
        bodys' <- forM kqq_bodys' $ \(_, qty, _, b) -> do
          conCheck ctx (Eq r qty)
              (hang 2 $ pp l <> text "return type of case mismatched in branch" <+>
               squotes (vardoc k) <+> text "of" <+>
               squotes (keyword "case" <+> pp p <+>
                        keyword "of" <+> text "...:") <$>
               text " expect" <+> squotes (pp r) <$>
               text "but got" <+> squotes (pp qty))
          return b
        return (r, r', bodys')
      return (r2, foldl C.App (C.Cast C.Down C.Weak (length as + 1) Nothing p') (r':bodys'))

    ConPi{} -> raise NoLoc "fatal: conpi"
    ConApp{} -> raise NoLoc "fatal: conapp"
    Any{} -> return (Any NoLoc, C.Any)
    Unit{} -> return (Unit NoLoc, C.Unit)
    Let _ b -> do
      ((x, Embed e1), e2) <- unbind b
      (t1, e1') <- transTm ctx e1
      let x' = transVar x
      (t2, e2') <- transTm (ctxAddVar (ctxAddBnd (ctxAddSBnd ctx x e1) x' e1') x x' t1) e2
      return (t2, C.Let (C.vnbind x' e1' e2'))
    LetRec l _ -> raise l "unexpected letrec, not desugared?"
    IfThenElse l e1 e2 e3 -> do
      (t1, e1') <- transTm ctx e1
      case t1 of
        LTy _ LTBool -> do
          (t2, e2') <- transTm ctx e2
          (t3, e3') <- transTm ctx e3
          conCheck ctx (Eq t2 t3)
            (hang 2 $ pp l <> text "if branches type mismatch in" <+>
             squotes (pp inp) <> colon <$>
             squotes (pp e2) <+> text "has type" <+> squotes (pp t2) <$>
             squotes (pp e3) <+> text "has type" <+> squotes (pp t3))
          return (t2, C.IfThenElse e1' e2' e3')
        _ -> throwError $ pp l <> text "if condition" <+> squotes (pp e1) <+>
                          text "is not a boolean, but" <+> squotes (pp t1)
    LTy _ ty -> return (Star NoLoc, C.LTy ty)
    LVal _ val -> return (LTy NoLoc (typeOfVal val), C.LVal val)
    LUnary l op tm -> do
      (ty, tm') <- transTm ctx tm
      ty' <- kindCheck ctx ty
      retTy <- litOpCheck l (typeOfUOp op) (ty, ty') ctx (inp, tm)
      return (LTy NoLoc retTy, C.LUnary op tm')
    LBin l op e1 e2 -> do
      let opTy = typeOfBOp op
      (t1, e1') <- transTm ctx e1
      (t2, e2') <- transTm ctx e2
      t1' <- kindCheck ctx t1
      t2' <- kindCheck ctx t2
      retTy <- litOpCheck l opTy (t1, t1') ctx (inp, e1)
      _ <- litOpCheck l opTy (t2, t2') ctx (inp, e2)
      return (LTy NoLoc retTy, C.LBin op e1' e2')

    _ -> throwError $ pp (getLoc inp) <> text "missing annotation in" <+> squotes (pp inp)
