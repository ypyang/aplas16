module Language.Fun.CodeGen (
  M,
  runM,
  Id(..),
  Tm(..),
  Bnd(..),
  varstr,
  toId,
  bindRep,
  eraseTm,
  eraseCtx
  ) where


import           Control.Applicative              ((<*>))
import           Control.Monad.Identity
import           Data.Char                        (isUpper)
import qualified Data.LinkedHashMap.IntMap        as HM
import qualified Language.Fun.Core                as C hiding (M, runM)
import           Language.Fun.Lit
import           Language.Fun.Pretty
import qualified Language.Fun.Typecheck           as T hiding (M, runM)
import           Prelude                          hiding ((<$>), (<*>))
import           Text.PrettyPrint.ANSI.Leijen     hiding (Pretty)
import           Unbound.Generics.LocallyNameless hiding (bind, subst)

-- Erased term
data Id = Id {
  jsName :: String,
  hsName :: String
  } deriving (Eq, Show)

data Tm = Var Id
        | Star
        | App Tm Tm
        | Lam Id Tm
        | Pi Id Tm Tm
        | Mu Id Tm
        | Any
        | Unit
        | Let Bnd Tm
        | IfThenElse Tm Tm Tm
        | LTy LitTy
        | LVal LitVal
        | LUnary LitUnaryOp Tm
        | LBin LitBinOp Tm Tm
        deriving (Eq, Show)

data Bnd = Bnd Id Tm deriving (Eq, Show)

clusterLam :: Tm -> ([String], Tm)
clusterLam (Lam x e) =
  let (vars, body) = clusterLam e
      v = jsName x
  in ((if null v then "_" else v) :vars, body)
clusterLam e = ([], e)

instance Pretty Tm where
  ppr p inp = case inp of
    Var x -> text (jsName x)
    Star -> char '*'
    App e1 e2 -> parensIf (p > 1) $ ppr 1 e1 <+> ppr 2 e2
    Lam{} -> let (xs', e') = clusterLam inp
                 bndDoc = strsep " " (map text xs')
             in parensIf (p > 0) $ backslash <> bndDoc <+> dot <+> pp e'
    Pi x t1 t2 -> let lDoc = if null (jsName x) then ppr 1 t1
                             else parens (text (jsName x) <+> colon <+> pp t1)
                  in parensIf (p > 0) $ lDoc <+> text "->" <+> pp t2
    Mu x e -> parensIf (p > 0) $ char '/' <> text (jsName x) <+> dot <+> pp e
    Any -> char '_'
    Unit -> text "()"
    Let (Bnd x e1) e2 -> parensIf (p > 0) $
      keyword "let" <+> text (jsName x) <+> equals <+> ppr p e1 <+>
      keyword "in" <+> pp e2
    IfThenElse e1 e2 e3 -> parensIf (p > 0) $
      keyword "if" <+> pp e1 <+> keyword "then" <+> pp e2 <+>
      keyword "else" <+> pp e3
    LTy ty -> pp ty
    LVal val -> pp val
    LUnary op a -> parensIf (p > 0) $ pp op <> parens (pp a)
    LBin op a b ->
      let ad = ppr (binPrec op) a
          bd = ppr (succ . binPrec $ op) b
      in parensIf (p > 0) $ ad <+> pp op <+> bd

instance Pretty Bnd where
  ppr p (Bnd x e) = keyword "def" <+> text (jsName x) <+> equals <+> ppr p e

type M = FreshMT Identity

varstr :: C.VarName -> String
varstr = name2String

runM :: M a -> a
runM = runIdentity . runFreshMT

bindRep :: (Tm -> Tm) -> Int -> Tm -> Tm
bindRep _ 0 e = e
bindRep f n e = f (bindRep f (n - 1) e)

toId :: String -> Id
toId v = Id { jsName = map (\c -> if c == '\'' then '$' else c) v,
              hsName = if null v then v
                       else if isUpper (head v)
                            then "_ctor_" ++ v else v }

eraseTm :: C.Tm -> M Tm
eraseTm inp = case inp of
  C.Var v -> return $ Var (toId $ varstr v)
  C.Star -> return Star
  C.App e1 e2 -> App `fmap` eraseTm e1 <*> eraseTm e2
  C.Lam b -> do
    ((x, _), e) <- unbind b
    Lam (toId $ varstr x) `fmap` eraseTm e
  C.Pi b -> do
    ((x, Embed t1), t2) <- unbind b
    Pi (toId $ varstr x) `fmap` eraseTm t1 <*> eraseTm t2
  C.Mu b -> do
    ((x, _), e) <- unbind b
    Mu (toId $ varstr x) `fmap` eraseTm e
  C.Cast _ _ _ _ e -> eraseTm e
  C.ConPi _ t -> eraseTm t
  C.ConLam _ e -> eraseTm e
  C.ConApp _ e -> eraseTm e
  C.Any -> return Any
  C.Unit -> return Unit
  C.Let b -> do
    ((x, Embed e1), e2) <- unbind b
    e1' <- eraseTm e1
    e2' <- eraseTm e2
    return $ Let (Bnd (toId $ varstr x) e1') e2'
  C.IfThenElse e1 e2 e3 ->
    IfThenElse `fmap` eraseTm e1 <*> eraseTm e2 <*> eraseTm e3
  C.LTy t -> return $ LTy t
  C.LVal v -> return $ LVal v
  C.LUnary op e -> LUnary op `fmap` eraseTm e
  C.LBin op e1 e2 -> LBin op `fmap` eraseTm e1 <*> eraseTm e2

eraseCtx :: T.Ctx -> M [Bnd]
eraseCtx ctx = mapM (\(v, e) ->
                      do e' <- eraseTm e
                         return $ Bnd (toId $ varstr v) e') (HM.toList $ T.bndCtx ctx)
