module Language.Fun.GenJS (
  jsGen,
  ) where


import           Language.Fun.CodeGen
import qualified Language.Fun.Core            as C hiding (M, runM)
import           Language.Fun.Lit
import           Language.Fun.Pretty
import qualified Language.Fun.Typecheck       as T hiding (M, runM)
import           Prelude                      hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen hiding (Pretty)

jsBaseClass :: Doc
jsBaseClass = text "new $Expr"

jsOpClass :: Doc
jsOpClass = text "$Op"

jsVarClass :: Doc
jsVarClass = text "$var"

jsUnaryOp :: LitUnaryOp -> Doc
jsUnaryOp op = case op of
  LOpShow -> text "show"
  LOpNeg -> text "neg"
  LOpNot -> text "not"

jsBinOp :: LitBinOp -> Doc
jsBinOp op = case op of
  LOpAdd -> text "plus"
  LOpMinus -> text "minus"
  LOpMul -> text "mul"
  LOpConcat -> text "concat"
  LOpG -> text "grt"
  LOpGE -> text "grteq"
  LOpL -> text "less"
  LOpLE -> text "lesseq"
  LOpEq -> text "eq"
  LOpNeq -> text "neq"
  LOpAnd -> text "and"
  LOpOr -> text "or"

jsGenTemplate :: Doc -> Doc
jsGenTemplate e = text "function" <> parens empty <>
  braces (text "var $t =" <+> parens e <> char ';' <+>
          text "return $genExpr($t);")

jsGenBnd :: Bnd -> Doc
jsGenBnd (Bnd x e) = text "var" <+> text (jsName x) <+> equals <+>
  jsGenTemplate (jsGenTm e) <> char ';'

jsGenBnds :: [Bnd] -> Doc
jsGenBnds = foldl (\v b -> v <> jsGenBnd b) empty

jsGenFunc :: Id -> Tm -> Doc
jsGenFunc x e = jsDocGenFunc x (jsGenTm e)

jsDocGenFunc :: Id -> Doc -> Doc
jsDocGenFunc x d = jsBaseClass <> parens
  (text "function" <> parens (text $ jsName x) <>
   braces (text "return" <+> parens d <> char ';'))

jsGenVal :: Doc -> Doc
jsGenVal v = jsBaseClass <> parens empty <> dot <> text "val" <> parens v

jsGenStr :: Tm -> Doc
jsGenStr = text . show . pp

jsGenRaw :: Tm -> Doc
jsGenRaw tm = braces (dquotes (text "raw") <>
                      colon <> (text . show . show . pp $ tm))

jsDocGenOp :: Doc -> Doc -> Doc
jsDocGenOp op d = jsOpClass <> dot <>
  op <> parens empty <> dot <> text "app" <>
  parens d

jsGenOp :: Doc -> [Tm] -> Doc
jsGenOp op es = jsDocGenOp op (strsep "," (map jsGenTm es))

jsGenTm :: Tm -> Doc
jsGenTm inp = case inp of
  Var x -> jsVarClass <> parens (text $ jsName x)
  Star -> jsGenVal (jsGenRaw inp)
  App e1 e2 -> jsGenTm e1 <> text ".app" <> parens (jsGenTm e2)
  Lam x e -> jsGenFunc x e
  Pi{} -> jsGenVal (jsGenRaw inp)
  Mu x e -> jsDocGenOp (text "fix")
      (text "function" <> parens empty <>
       braces (text "return" <+>
               parens (jsGenTm (Lam x e)) <> char ';'))
  Any -> jsGenVal (jsGenRaw inp)
  Unit -> jsGenVal (text "null")
  Let b e -> jsBaseClass <> parens
    (text "function" <> parens empty <>
     braces (jsGenBnd b <> text "return" <+>
             parens (jsGenTm e) <> char ';')) <>
    dot <> text "app()"
  IfThenElse e1 e2 e3 -> jsGenOp (text "ifthenelse") [e1, e2, e3]
  LTy{} -> jsGenVal (jsGenRaw inp)
  LVal (LVBool x) -> jsGenVal (text $ if x then "true" else "false")
  LVal{} -> jsGenVal (jsGenStr inp)
  LUnary op e -> jsGenOp (jsUnaryOp op) [e]
  LBin op e1 e2 -> jsGenOp (jsBinOp op) [e1, e2]

jsGen :: T.Ctx -> C.Tm -> M (Doc, Doc, Doc)
jsGen ctx tm = do
  bnds <- eraseCtx ctx
  e <- eraseTm tm
  return (jsGenBnds bnds, jsGenTm e,
          text "/*" <>
          (if null bnds then empty
           else line <> strvsep ";" (map pp bnds)) <> text " ;" <$>
          pp e <$> text "*/" <> line)
