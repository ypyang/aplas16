{-# LANGUAGE DeriveDataTypeable    #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE FlexibleInstances     #-}
{-# LANGUAGE MultiParamTypeClasses #-}
module Language.Fun.Src (
  Ty,
  Tm(..),
  Decl(..),
  Prog(..),
  Tele,
  DataArgs(..),
  CastTy(..),
  CastAnn(..),
  teleToBind,
  Con(..),
  Bnd,
  vnbind,
  VarName,
  flattenApp,
  substVec,
  clusterConPi,
  clusterPi,
  M,
  runM,
  tmpName,
  tmpNameList,
  tmpVarWith,
  newBind,
  vardoc,
  extractArgNames,
  extractArgTypes,
  arrowToTeles,
  dataArgsToTele,
  vnSubst,
  tmpVar,
  dummyvar,
  newVar,
  bindAll,
  teleToRebind,
  isDummyVar,
  tmpVarList,
  IBnd,
  vnibind,
  teleToIBind,
  newIBind,
  idToIBind,
  Loc(..),
  getLoc,
  getLocDecl,
  ) where

import           Control.Monad.Identity
import           Data.Hashable                                  (Hashable,
                                                                 hashWithSalt)
import qualified Data.Set                                       as S
import           Data.Typeable                                  (Typeable)
import           GHC.Generics
import           Language.Fun.Lit
import           Language.Fun.Pretty
import           Prelude                                        hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen                   hiding (Pretty)
import           Unbound.Generics.LocallyNameless
import           Unbound.Generics.LocallyNameless.Internal.Fold (toListOf)
import           Data.Function                                  (on)

-- Unbound library
type VarName = Name Tm
type ModName = [String]
type Bnd = Bind (VarName, Embed Ty) Tm
type IBnd = Bind VarName Tm
type Alt = (VarName, Bind [VarName] Tm)

-- Expression
data Loc = NoLoc | Located Int Int deriving (Show, Generic, Typeable)
data Con = Eq Ty Ty deriving (Show, Generic, Typeable)
data CastTy = Up | Down deriving (Show, Generic, Typeable, Eq)
data CastAnn = Weak | Full deriving (Show, Generic, Typeable, Eq)
type Ty = Tm
data Tm = Var Loc VarName
        | Star Loc
          -- Standard syntax
        | App Loc Tm Tm
        | Lam Loc IBnd
        | LamAnn Loc Bnd
        | Pi Loc Bnd
        | Mu Loc IBnd
        | Cast Loc CastTy CastAnn Int Tm
        | Case Loc Tm [Alt] -- case e of {K {x} => e'}
        | ConPi Loc [Con] Ty
        | ConLam Loc Int Tm
        | ConApp Loc Int Tm
          -- Extended syntax
        | Any Loc
        | Unit Loc
        | Let Loc Bnd
        | LetRec Loc Bnd
        | IfThenElse Loc Tm Tm Tm
        | Anno Loc Tm Ty
          -- Literal
        | LTy Loc LitTy
        | LVal Loc LitVal
        | LUnary Loc LitUnaryOp Tm
        | LBin Loc LitBinOp Tm Tm
        deriving (Show, Generic, Typeable)

-- Declaration
type Tele = (VarName, Ty)
data DataArgs = EmptyArgs
              | ConsArgs (Rebind (VarName, Embed Ty) DataArgs)
              deriving (Show, Generic, Typeable)
data Decl = Data Loc VarName (Bind DataArgs [Tele])             -- data D {a : S} = {K : {b : T} -> D {E}}
          | DataSimp Loc VarName (Bind DataArgs [(VarName, [Tele])]) -- data D {a : S} = {K {x : T}}
          | Rec Loc VarName (Bind DataArgs (VarName, [Tele]))   -- data D {a : S} = K {b : T}
          | VarDef Loc VarName Tm Ty
          | VarDefInf Loc VarName Tm
          | VarRec Loc VarName Tm Ty
          | Import Loc Bool ModName (Maybe ModName) [VarName] -- import explicit? M as M' (...)
          | Export Loc ModName [VarName] -- module M (...)
          deriving (Show)

-- Program
data Prog = Prog [Decl] Tm deriving (Show)

-- Unbound library instances
instance Alpha Tm where
  aeq' _ (Any _) _ = True
  aeq' _ _ (Any _) = True
  aeq' c a b = ((gaeq c) `on` from) a b

instance Alpha Loc where
  aeq' _ _ _ = True

instance Alpha Con
instance Alpha CastTy
instance Alpha CastAnn
instance Alpha DataArgs

instance Subst Tm LitTy
instance Subst Tm LitVal
instance Subst Tm LitBinOp
instance Subst Tm LitUnaryOp
instance Subst Tm Con
instance Subst Tm Loc
instance Subst Tm CastTy
instance Subst Tm CastAnn
instance Subst Tm DataArgs

instance Subst Tm Tm where
  isvar (Var _ v) = Just (SubstName v)
  isvar _  = Nothing

instance Hashable (Name Tm) where
  hashWithSalt x v = hashWithSalt x (show v)

-- Utility functions
type M = FreshMT Identity

runM :: M a -> a
runM = runIdentity . runFreshMT

dummyvar :: VarName
dummyvar = newVar ""

vnbind :: VarName -> Ty -> Tm -> Bnd
vnbind x t = bind (x, Embed t)

vnibind :: VarName -> Tm -> IBnd
vnibind = bind

isFvOf :: VarName -> Tm -> Bool
isFvOf v e = v `S.member` fvSet e

teleToBind :: (Bnd -> Tm) -> Tm -> [Tele] -> Tm
teleToBind f = foldr (\(v, t) tm -> f (vnbind v t tm))

teleToIBind :: (IBnd -> Tm) -> Tm -> [Tele] -> Tm
teleToIBind f = foldr (\(v, _) tm -> f (bind v tm))

idToIBind :: (IBnd -> Tm) -> Tm -> [VarName] -> Tm
idToIBind f = foldr (\v tm -> f (bind v tm))

newVar :: String -> VarName
newVar = string2Name

newBind :: String -> Ty -> Tm -> Bnd
newBind x t = bind (newVar x, Embed t)

newIBind :: String -> Ty -> IBnd
newIBind x = bind (newVar x)

bindAll :: (Alpha p, Alpha t) => p -> t -> Bind p t
bindAll = bind

teleToRebind :: Tele -> DataArgs -> Rebind (VarName, Embed Ty) DataArgs
teleToRebind x = rebind (fst x, Embed (snd x))

clusterLam :: Tm -> M ([VarName], Tm)
clusterLam (Lam _ b) = do
  (n, e) <- unbind b
  (vars, body) <- clusterLam e
  return (n : vars, body)
clusterLam e = return ([], e)

clusterPi :: Tm -> M ([Tele], Tm)
clusterPi (Pi _ b) = do
  ((n, Embed t), e) <- unbind b
  (vars, body) <- clusterPi e
  return ((n, t) : vars, body)
clusterPi e = return ([], e)

clusterLamAnn :: Tm -> M ([Tele], Tm)
clusterLamAnn (LamAnn _ b) = do
  ((n, Embed t), e) <- unbind b
  (vars, body) <- clusterLamAnn e
  return ((n, t) : vars, body)
clusterLamAnn e = return ([], e)

clusterConPi :: Tm -> ([Con], Tm)
clusterConPi (ConPi _ c e) =
  let (cons, body) = clusterConPi e
  in (c ++ cons, body)
clusterConPi e = ([], e)

extractArgNames :: DataArgs -> [VarName]
extractArgNames EmptyArgs = []
extractArgNames (ConsArgs bnd) =
  let ((v, _), args) = unrebind bnd
  in v : extractArgNames args

extractArgTypes :: DataArgs -> [Ty]
extractArgTypes EmptyArgs = []
extractArgTypes (ConsArgs bnd) =
  let ((_, Embed ty), args) = unrebind bnd
  in ty : extractArgTypes args

dataArgsToTele :: DataArgs -> [Tele]
dataArgsToTele EmptyArgs = []
dataArgsToTele (ConsArgs bnd) =
  let ((v, Embed ty), args) = unrebind bnd
  in (v, ty) : dataArgsToTele args

arrowToTeles :: [Ty] -> [Tele]
arrowToTeles = map (\t -> (dummyvar, t))

flattenApp :: Tm -> [Tm]
flattenApp (App _ f a) = flattenApp f ++ [a]
flattenApp e = [e]

vnSubst :: VarName -> Tm -> Tm -> Tm
vnSubst = subst

substVec :: Tm -> [Tele] -> Tm
substVec = foldl (\v (x, e) -> vnSubst x e v)

fvSet :: Tm -> S.Set VarName
fvSet = S.fromList . toListOf fv

tmpVar :: VarName -> VarName
tmpVar = tmpVarWith ""

tmpVarWith :: String -> VarName -> VarName
tmpVarWith x y = string2Name $ tmpName (x ++ name2String y)

tmpVarList :: String -> [VarName]
tmpVarList x = map (\s -> if null s then dummyvar else string2Name s) (tmpNameList x)

isDummyVar :: VarName -> Bool
isDummyVar v = v == dummyvar || null (name2String v)

getLoc :: Tm -> Loc
getLoc inp = case inp of
  Var l _ -> l
  Star l -> l
  App l _ _ -> l
  Lam l _ -> l
  LamAnn l _ -> l
  Pi l _ -> l
  Mu l _ -> l
  Cast l _ _ _ _ -> l
  Case l _ _ -> l
  ConPi l _ _ -> l
  ConLam l _ _ -> l
  ConApp l _ _ -> l
  Any l -> l
  Unit l -> l
  Let l _ -> l
  LetRec l _ -> l
  IfThenElse l _ _ _ -> l
  Anno l _ _ -> l
  LTy l _ -> l
  LVal l _ -> l
  LUnary l _ _ -> l
  LBin l _ _ _ -> l

getLocDecl :: Decl -> Loc
getLocDecl inp = case inp of
   Data l _ _  -> l
   DataSimp l _ _ -> l
   Rec l _ _ -> l
   VarDef l _ _ _ -> l
   VarDefInf l _ _ -> l
   VarRec l _ _ _ -> l
   Import l _ _ _ _ -> l
   Export l _ _ -> l

-- Pretty print
instance Pretty Tm where
  ppr p = runM . pprM p

instance Pretty Con where
  ppr _ = runM . ppMCon

instance Pretty Loc where
  ppr _ NoLoc = empty
  ppr _ (Located row col) = dullblue . bold $
    int row <> colon <> int col <> colon <> space

instance Pretty Decl where
  ppr _ = runM . ppMDecl

instance Pretty [Decl] where
  ppr _ ds = runM . fmap (strvsep ";") $ mapM ppMDecl ds

instance Pretty Prog where
  ppr _ (Prog ds tm) = (if null ds then empty
                        else pp ds <+> char ';' <> line) <> pp tm

vardoc :: VarName -> Doc
vardoc v = if isDummyVar v then char '_' else text (name2String v)

opdoc :: VarName -> Doc
opdoc v = case name2String v of
  'O':'p':'(':op' -> text (init op')
  _ -> vardoc v

ppM :: Tm -> M Doc
ppM = pprM 0

ppMCon :: Con -> M Doc
ppMCon (Eq t t') = do
  td <- ppM t
  td' <- ppM t'
  return $ td <+> char '~' <+> td'

ppMCastTy :: CastTy -> M Doc
ppMCastTy ty = return . text $ case ty of
  Up -> "castup"
  Down -> "castdn"

ppMCastAnn :: CastAnn -> M Doc
ppMCastAnn ty = return . text $ case ty of
  Weak -> ""
  Full -> "!"

ppMDataArgs :: DataArgs -> M [Doc]
ppMDataArgs EmptyArgs = return []
ppMDataArgs (ConsArgs bnd) = do
  let ((a, Embed ty), left) = unrebind bnd
  tyDoc <- ppM ty
  leftDoc <- ppMDataArgs left
  return $ parens (vardoc a <+> colon <+> tyDoc) : leftDoc

ppMDataDef :: (VarName, DataArgs) -> M Doc
ppMDataDef (name, args) = do
  argsDoc <- ppMDataArgs args
  return $ keyword "data" <+> opdoc name <>
    ppIf argsDoc (strsep " " argsDoc)

ppMTele :: Tele -> M Doc
ppMTele (name, ty) = do
  tyDoc <- ppM ty
  return $ vardoc name <+> colon <+> tyDoc

ppMTele2 :: Tele -> M Doc
ppMTele2 inp@(name, ty) =
  if isDummyVar name then pprM 2 ty
  else fmap parens (ppMTele inp)

ppMTeles :: [Tele] -> M [Doc]
ppMTeles = mapM ppMTele

ppIf :: [a] -> Doc -> Doc
ppIf xs d = if null xs then empty
            else space <> d

ppMDecl :: Decl -> M Doc
ppMDecl decl = case decl of
  Data _ d databind -> do
    (hd, bd) <- unbind databind
    headDoc <- ppMDataDef (d, hd)
    bodyDocs <- ppMTeles bd
    return . hang 2  $
      headDoc <> ppIf bodyDocs (equals <+> strspsep "|" bodyDocs)
  DataSimp _ d simpbind -> do
    (hd, bd) <- unbind simpbind
    headDoc <- ppMDataDef (d, hd)
    bodyDocs <- forM bd $ \(name, tys) -> do
      tysDocs <- mapM ppMTele2 tys
      return $ vardoc name <> ppIf tysDocs (strsep " " tysDocs)
    return . hang 2 $
      headDoc <> ppIf bodyDocs (equals <+> strspsep "|" bodyDocs)
  Rec _ d recbind -> do
    (hd, (name, bd)) <- unbind recbind
    headDoc <- ppMDataDef (d, hd)
    bodyDocs <- ppMTeles bd
    return . hang 2  $
      headDoc <+> equals <+> vardoc name <+>
      lbrace <+> strspsep "," bodyDocs <+> rbrace
  VarDef _ v ty tm -> ppDef "def" v ty tm
  VarRec _ v ty tm -> ppDef "defrec" v ty tm
  VarDefInf _ v tm -> do
    tmDoc <- ppM tm
    return $ keyword "def" <+> opdoc v <+> equals <+> tmDoc

  Import _ e m asm vars -> do
    let expDoc = if e then space <> text "explicit" else empty
        asmDoc = maybe empty
                 (\a -> space <> text "as" <+> ppMod a) asm
        varsDoc = ppVars vars
    return $ text "import" <> expDoc <+> ppMod m <> asmDoc <> varsDoc
  Export _ m vars ->
    return $ text "module" <+> ppMod m <> ppVars vars
    
  where ppDef n v ty tm = do
          tmDoc <- ppM tm
          tyDoc <- ppM ty
          return $
            keyword n <+> opdoc v <+>
            colon <+> tyDoc <+> equals <+>
            tmDoc
        ppMod xs = strsep "." (map text xs)
        ppVars vars = if null vars then empty 
                      else space <> parens (strsep ", " $ map vardoc vars)

pprMApp :: Int -> Tm -> Tm -> M Doc
pprMApp p a b = do
  ad <- pprM 1 a
  bd <- pprM 2 b
  return . parensIf (p > 1) $ ad <+> bd 

pprM :: Int -> Tm -> M Doc
pprM p inp = case inp of
  Var _ n -> return $ vardoc n
  Star _ -> return $ char '*'
  App _ e1@(App _ (Var _ v) a) b ->
    case name2String v of
      'O':'p':'(':op' -> do
        let op = init op'
        ad <- pprM (binPrec LOpAnd) a
        bd <- pprM (succ . binPrec $ LOpAnd) b
        return . parensIf (p > 0) $ ad <+> text op <+> bd
      _ -> pprMApp p e1 b
  App _ a b -> pprMApp p a b
  Lam{} -> clusterLam inp >>= \(vars, body) -> do
    let varsDoc = map vardoc vars
    bodyDoc <- ppM body
    return . parensIf (p > 0) $
      backslash <> hsep varsDoc <+> dot <+> bodyDoc
  LamAnn{} -> clusterLamAnn inp >>= \(vars, body) -> do
    varsDoc <- forM vars $ \(n, t) -> do
      varDoc <- ppM t
      return . parensIf (length vars > 1) $
        vardoc n <+> colon <+> varDoc
    bodyDoc <- ppM body
    return . parensIf (p > 0) $
      backslash <> hsep varsDoc <+> dot <+> bodyDoc
  Pi _ bnd -> do
    ((n, Embed t1), t2) <- unbind bnd
    firstDoc <- if isDummyVar n || not (n `isFvOf` t2) then pprM 1 t1 -- Non-dependent Pi
                else do d <- ppM t1
                        return $ parens (vardoc n <+> colon <+> d)
    secondDoc <- ppM t2
    return . parensIf (p > 0) $ firstDoc <+> text "->" <+> secondDoc
  Mu _ bnd -> do
    (n, e) <- unbind bnd
    ed <- ppM e
    return . parensIf (p > 0) $
      char '/' <> vardoc n <+> dot <+> ed
  Cast _ ty ann s e -> if s == 0 then pprM p e else do
    tyd <- ppMCastTy ty
    annd <- ppMCastAnn ann
    ed <- pprM 1 e
    return . parensIf (p > 0) $
      tyd <> annd <> (if s == 1 then empty else
                        char '^' <> (if s < 0 then parens else id) (int s))
      <+> ed
  Case _ tm alts -> do
    let ppMAlt (k, bs) = do
          (xs, e) <- unbind bs
          let xsDoc = map vardoc xs
          eDoc <- ppM e
          return $ vardoc k <>
            (if null xsDoc then empty
             else space <> strsep " " xsDoc) <+>
            text "=>" <+> eDoc
    altsDoc <- fmap (strspsep "|") (mapM ppMAlt alts)
    tmDoc <- ppM tm
    return . parensIf (p > 0) . hang 2 $
      keyword "case" <+> tmDoc <+>
      keyword "of" <+> altsDoc
  ConPi{} -> let (cons, body) = clusterConPi inp in do
    consDoc <- do
      ds <- mapM ppMCon cons
      return . parens $ strsep ", " ds
    bodyDoc <- ppM body
    return . parensIf (p > 0) $ consDoc <+> text "=>" <+> bodyDoc
  ConLam _ s e -> do
    bodyDoc <- ppM e
    return . parensIf (p > 0) $ text "\\~" <>
      (if s > 1 then char '^' <> int s else empty) <+>
      dot <+> bodyDoc
  ConApp _ s e -> do
    ed <- ppM e
    return . parensIf (p > 0) $ char '#' <>
      (if s > 1 then char '^' <> int s else empty) <>
      parens ed

  Any _ -> return $ char '_'
  Unit _ -> return $ text "()"
  Let _ b -> ppLet "let" b
  LetRec _ b -> ppLet "letrec" b
  IfThenElse _ e1 e2 e3 -> do
    d1 <- ppM e1
    d2 <- ppM e2
    d3 <- ppM e3
    return . parensIf (p > 0) $
      keyword "if" <+> d1 <+> keyword "then" <+>
      d2 <+> keyword "else" <+> d3
  Anno _ e t -> do
    eDoc <- pprM 1 e
    tDoc <- ppM t
    return . parensIf (p > 0) $
      eDoc <+> text "::" <+> tDoc

  LTy _ ty -> return $ pp ty
  LVal _ val -> return $ pp val
  LUnary _ op a -> do
    ad <- ppM a
    return . parensIf (p > 0) $ pp op <> parens ad
  LBin _ op a b -> do
    ad <- pprM (binPrec op) a
    bd <- pprM (succ . binPrec $ op) b
    return . parensIf (p > 0) $ ad <+> pp op <+> bd

  where ppLet kw bv = do
          ((v, Embed tm), e) <- unbind bv
          tmDoc <- ppM tm
          eDoc <- ppM e
          return . parensIf (p > 0) $
            keyword kw <+> vardoc v <+> equals <+>
            tmDoc <+> keyword "in" <+> eDoc

