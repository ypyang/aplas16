module Language.Fun.Desugar (
  runM,
  desugarDecl,
  desugar,
  desugarProg,
  ) where

import           Control.Monad.Except
import           Control.Monad.Identity
import           Language.Fun.Src                 hiding (M, runM)
import           Prelude                          hiding ((<$>), (<*>))
import           Text.PrettyPrint.ANSI.Leijen     hiding (Pretty)
import           Unbound.Generics.LocallyNameless hiding (subst)
import           Control.Applicative              ((<*>))

type M = ExceptT Doc (FreshMT Identity)

runM :: M a -> Either Doc a
runM = runIdentity . runFreshMT . runExceptT

desugarBnd :: (Bnd -> Tm) -> Bnd -> M Tm
desugarBnd f b = do
  ((x, Embed t), e) <- unbind b
  t' <- desugar t
  e' <- desugar e
  return $ f (vnbind x t' e')

desugarIBnd :: (IBnd -> Tm) -> IBnd -> M Tm
desugarIBnd f b = do
  (x, e) <- unbind b
  e' <- desugar e
  return $ f (vnibind x e')

desugarCon :: Con -> M Con
desugarCon (Eq ty1 ty2) = Eq `fmap` desugar ty1 <*> desugar ty2

desugarConBnd :: ([Con] -> Tm -> Tm) -> [Con] -> Tm -> M Tm
desugarConBnd f cons e = f `fmap` mapM desugarCon cons <*> desugar e

desugar :: Tm -> M Tm
desugar inp = case inp of
  Var{} -> return inp
  Star _ -> return inp
  App l e1 e2 -> App l `fmap` desugar e1 <*> desugar e2
  Lam l b -> desugarIBnd (Lam l) b
  LamAnn l b -> desugarBnd (LamAnn l) b
  Pi l b -> desugarBnd (Pi l) b
  Mu l b -> desugarIBnd (Mu l) b
  Cast l updown ann step tm -> do
    tm' <- desugar tm
    return $ Cast l updown ann step tm'
  Case l tm alts -> Case l `fmap` desugar tm <*>
    forM alts (\(v, xs_e) -> do
                  (xs, e) <- unbind xs_e
                  e' <- desugar e
                  return (v, bind xs e'))
  ConPi l cons ty -> desugarConBnd (ConPi l) cons ty
  ConLam l step tm -> ConLam l step `fmap` desugar tm
  ConApp l step tm -> ConApp l step `fmap` desugar tm
  Any _ -> return inp
  Unit _ -> return inp
  Let l b -> desugarBnd (Let l) b
  LetRec l b -> do -- letrec x = e1 in e2 ~> let x = /x.e1 in e2
    ((x, Embed e1), e2) <- unbind b
    e1' <- desugar e1
    e2' <- desugar e2
    return $ case e1' of
      Anno _ tm ty -> Let l (vnbind x (Anno NoLoc (Mu NoLoc (vnibind x tm)) ty) e2')
      _ -> Let l (vnbind x (Mu NoLoc (vnibind x e1')) e2')
  IfThenElse l e1 e2 e3 -> IfThenElse l `fmap` desugar e1 <*> desugar e2 <*> desugar e3
  Anno l e t -> Anno l `fmap` desugar e <*> desugar t
  LTy{} -> return inp
  LVal{} -> return inp
  LUnary l op tm -> LUnary l op `fmap` desugar tm
  LBin l op e1 e2 -> LBin l op `fmap` desugar e1 <*> desugar e2

desugarTele :: Tele -> M Tele
desugarTele (v, ty) = do
  ty' <- desugar ty
  return (v, ty')

desugarTeles :: [Tele] -> M [Tele]
desugarTeles = mapM desugarTele

desugarDataArgs :: DataArgs -> M DataArgs
desugarDataArgs EmptyArgs = return EmptyArgs
desugarDataArgs (ConsArgs bnd) = do
  let ((a, Embed ty), left) = unrebind bnd
  ty' <- desugar ty
  left' <- desugarDataArgs left
  return (ConsArgs $ rebind (a, Embed ty') left')

nameCheck :: VarName -> [VarName] -> M ()
nameCheck _ [] = return ()
nameCheck dataty (ctor:ctors) =
  if dataty == ctor then
    throwError (text "constructor" <+>
                squotes (vardoc ctor) <+>
                text "should be different from datatype" <+>
                squotes (vardoc dataty))
  else nameCheck dataty ctors

desugarDecl :: Decl -> M [Decl]
desugarDecl inp = case inp of
  Data l d databinds -> do
    (args, bd) <- unbind databinds
    args' <- desugarDataArgs args
    bd' <- desugarTeles bd
    nameCheck d (map fst bd)
    return [Data l d $ bind args' bd']
    
  DataSimp l d simpbinds -> do
    (args, bd) <- unbind simpbinds
    args' <- desugarDataArgs args
    let argNames = extractArgNames args
        retTy = foldl (App NoLoc) (Var NoLoc d) (map (Var NoLoc) argNames)
    bd' <- forM bd $ \(ctor, tys) -> do
      tys' <- desugarTeles tys
      return (ctor, teleToBind (Pi NoLoc) retTy tys')
    nameCheck d (map fst bd)
    return [Data l d $ bind args' bd']
  
  Rec l d recbind -> do
    (args, (k, bd)) <- unbind recbind
    nameCheck d [k]
    decl'@(Data _ d' bnd') <- fmap head $ desugarDecl (DataSimp NoLoc d (bind args [(k, map (\(_, b) -> (dummyvar, b)) bd)]))
    (args', (k', ty'):_) <- unbind bnd'
    (fieldTeles, dataRetTy) <- lift . clusterPi $ ty'
    let argTeles = dataArgsToTele args'
        fieldTmpVars = map (tmpVarWith "x" . fst) bd
        fieldVars = map fst bd
        yvar = tmpVarWith "y" d' 
        mkField xvar fieldTy =
          teleToBind (LamAnn NoLoc) ((LamAnn NoLoc) (vnbind yvar dataRetTy
                               ((Anno NoLoc) ((Case NoLoc) ((Var NoLoc) yvar)
                                [(k', bind fieldTmpVars ((Var NoLoc) xvar))]) fieldTy))) argTeles
        fieldDefs = zipWith mkField fieldTmpVars (map snd fieldTeles)
    return $ decl' : zipWith (VarDefInf NoLoc) fieldVars fieldDefs

  VarDef l v t e -> do
    t' <- desugar t
    e' <- desugar e
    return [VarDefInf l v (Anno NoLoc e' t')]
  VarDefInf l v tm -> do
    tm' <- desugar tm
    return [VarDefInf l v tm']
  VarRec l v t e -> do
    t' <- desugar t
    e' <- desugar e
    return [VarDefInf l v (Anno NoLoc (Mu NoLoc (vnibind v e')) t')]

  Import{} -> return [inp]
  Export{} -> return [inp]

desugarProg :: Prog -> M Prog
desugarProg (Prog ds e) = (Prog . concat) `fmap` mapM desugarDecl ds <*> desugar e
