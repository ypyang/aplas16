module Language.Fun.GenHS (
  hsGen,
  ) where


import           Language.Fun.CodeGen
import qualified Language.Fun.Core            as C hiding (M, runM)
import           Language.Fun.Lit
import           Language.Fun.Pretty
import qualified Language.Fun.Typecheck       as T hiding (M, runM)
import           Prelude                      hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen hiding (Pretty)

hsUnaryOp :: LitUnaryOp -> Doc -> Doc
hsUnaryOp op e =
  let opDoc = case op of
        LOpShow -> text "show"
        LOpNeg -> text "negate"
        LOpNot -> text "not"
      (inTy, _) = typeOfUOp op
      inTyDoc = pp inTy
  in opDoc <+> parens (e <+> text "::" <+> inTyDoc)

hsBinOp :: LitBinOp -> Doc -> Doc -> Doc
hsBinOp op a b =
  let opDoc = case op of
        LOpMul -> text "*"
        _ -> pp op
      (inTy, _) = typeOfBOp op
      inTyDoc = pp inTy
      ann = text "::"
  in parens (a <+> ann <+> inTyDoc) <+>
     opDoc <+>
     parens (b <+> ann <+> inTyDoc)

hsGenBnd :: Bnd -> Doc
hsGenBnd (Bnd x e) = text (hsName x) <+> equals <+> hsGenTm e

hsGenPrelude :: [Bnd] -> Doc
hsGenPrelude bnds = text "module Main where" <$>
  text "import Prelude hiding" <+>
    parens (strsep ", " $ map (\(Bnd x _) -> text (hsName x)) bnds) <$>
  text "import Unsafe.Coerce (unsafeCoerce)" <$>
  text "infixr 0 #" <$>
  text "(#) = unsafeCoerce"

hsGenBnds :: [Bnd] -> Doc
hsGenBnds bnds = hsGenPrelude bnds <$> vsep (map hsGenBnd bnds)

hsGenFunc :: Id -> Tm -> Doc
hsGenFunc x e = parens $
  text "\\" <> text (if null v then "_" else v) <+>
  text "->" <+> hsGenTm e
  where v = hsName x

hsGenStr :: Tm -> Doc
hsGenStr = text . show . pp

hsGenRaw :: Tm -> Doc
hsGenRaw = text . show . show . pp

hsGenTm :: Tm -> Doc
hsGenTm inp = case inp of
  Var x -> text (hsName x)
  Star -> hsGenRaw inp
  App e1 e2 -> parens $ hsGenTm e1 <+> char '#' <+> hsGenTm e2
  Lam x e -> hsGenFunc x e
  Pi{} -> hsGenRaw inp
  Mu x e -> parens $ hsGenTm (Let (Bnd x e) (Var x))
  Any -> text "undefined"
  Unit -> hsGenStr inp
  Let (Bnd x e1) e2 -> parens $
    text "let" <+> text (hsName x) <+>
    equals <+> hsGenTm e1 <+>
    text "in" <+> hsGenTm e2
  IfThenElse e1 e2 e3 -> parens $
    text "if" <+> hsGenTm e1 <+>
    text "then" <+> hsGenTm e2 <+>
    text "else" <+> hsGenTm e3
  LTy{} -> hsGenRaw inp
  LVal{} -> hsGenStr inp
  LUnary op e -> parens $ hsUnaryOp op (hsGenTm e)
  LBin op e1 e2 -> parens $ hsBinOp op (hsGenTm e1) (hsGenTm e2)

hsGen :: T.Ctx -> C.Tm -> M (Doc, Doc, Doc)
hsGen ctx tm = do
  bnds <- eraseCtx ctx
  e <- eraseTm tm
  return (hsGenBnds bnds, hsGenTm e,
          text "{-" <>
          (if null bnds then empty
           else line <> vsep (map pp bnds)) <$>
          pp e <$> text "-}" <> line)
