--> 2
{-

A polymorphic list and the `length` function

-}


data List a = Nil | Cons a (List a);



defrec length : (a : *) -> List a -> Int =
  \ a : * . \ l : List a . 
    case l of 
      Nil => 0
    | Cons x xs => 1 + length a xs ;

def test = Cons Int 1 (Cons Int 2 (Nil Int));
length Int test     -- return 2

