--> "2 3"
{-

Encoding length-indexed list (vector) by Leibniz equality

Note that in examples of the extended paper, for the purpose of presentation,
we use a *single* full-reduction cast by using reduction steps of the *surface
language*. However, the number of full casts here is **more than one**.

Because, in order to simplify the implementation, we first
translate terms into the core language, and check the reduction
relation. Because of encoding, datatypes are not primitives which
require more reduction steps.

-}

data Eq k (a : k) (b : k) = Proof {subst : (f : k -> *) -> f a -> f b};

def Sym (k : *) (f : k -> k -> *) (a : k) (b : k) = f b a;
def From (a : *) (b : *) = a -> b;
def To (a : *) (b : *) = b -> a;
def Lift (k : *) (k' : *) (f : k -> k') (a : k) (b : k) = Eq k' (f a) (f b);
def Lift2 (k : *) (f : k -> k -> k) (c : k) (a : k) (b : k) = Eq k (f a c) (f b c);

def refl : (k : *) -> (a : k) -> Eq k a a = 
    \k a . Proof k a a (\f x . x);

def sym : (k : *) -> (a : k) -> (b : k) -> Eq k a b -> Eq k b a =
    \k a b p . castdn^4 (subst k a b p (Sym k (Eq k) a) (castup^4 (refl k a)));

def trans : (k : *) -> (a : k) -> (b : k) -> (c : k) -> Eq k a b -> Eq k b c -> Eq k a c =
    \k a b c p q . subst k b c q (Eq k a) p;

def from : (a : *) -> (b : *) -> Eq * a b -> a -> b =
    \a b p . castdn^2 (subst * a b p (From a) (castup^2 (\x : a . x)));

def to : (a : *) -> (b : *) -> Eq * a b -> b -> a =
    \a b p . castdn^2 (subst * a b p (To a) (castup^2 (\x : a . x)));

def lift : (k : *) -> (k' : *) -> (a : k) -> (b : k) -> (f : k -> k') -> Eq k a b -> Eq k' (f a) (f b) =
    \k k' a b f p . castdn^5 (subst k a b p (Lift k k' f a) (castup^5 (refl k' (f a))));

def lift2 : (k : *) -> (a : k) -> (b : k) -> (c : k) -> (f : k -> k -> k) -> Eq k a b -> Eq k (f a c) (f b c) =
    \k a b c f p . castdn^5 (subst k a b p (Lift2 k f c a) (castup^5 (refl k (f a c))));

data Nat = Z | S Nat;
data Vec a (n : Nat) =
    Nil (Eq Nat n Z) | 
    Cons (m : Nat) (Eq Nat n (S m)) a (Vec a m);

def predNat : Nat -> Nat =
    \n . case n of S m => m;

def injNat : (n : Nat) -> (m : Nat) -> Eq Nat (S n) (S m) -> Eq Nat n m =
    \n m p . castdn!^6 (lift Nat Nat (S n) (S m) predNat p);

def castVec : (a : *) -> (n : Nat) -> (m : Nat) -> Eq Nat n m -> Vec a m -> Vec a n =
    \a n m p . to (Vec a n) (Vec a m) (lift Nat * n m (Vec a) p);

def head : (a : *) -> (n : Nat) -> (v : Vec a (S n)) -> a =
    \a n v . case v of
        Cons m p x xs => x;

def tail : (a : *) -> (n : Nat) -> (v : Vec a (S n)) -> Vec a n =
    \a n v . case v of
        Cons m p x xs => castVec a n m (injNat n m p) xs;

defrec plus (x : Nat) (y : Nat) : Nat =
  case x of
    Z => y |
    S x' => S (plus x' y);

def pf : (n : Nat) -> Eq Nat n n = \n . refl Nat n;

def castVecPlus : (a : *) -> (m : Nat) -> (n : Nat) -> (n' : Nat) -> Eq Nat n n' -> Vec a (plus n' m) -> Vec a (plus n m) =
    \a m n n' p . to (Vec a (plus n m)) (Vec a (plus n' m)) (lift Nat * (plus n m) (plus n' m) (Vec a) (lift2 Nat n n' m plus p));

defrec append (a : *) (n : Nat) (m : Nat) (xs : Vec a n) (ys : Vec a m) : Vec a (plus n m) =
    case xs of
        Nil p => castVecPlus a m n Z p (castup!^6 ys :: Vec a (plus Z m)) |
        Cons n' p x xs' => castVecPlus a m n (S n') p (castup!^8 Cons a (S (plus n' m)) (plus n' m) (pf (S (plus n' m))) x (append a n' m xs' ys) :: Vec a (plus (S n') m));

def vec1 = Cons Int (S Z) Z (pf (S Z)) 3 (Nil Int Z (pf Z)); -- <3>
def vec2 = Cons Int (S (S Z)) (S Z) (pf (S (S Z))) 1 (Cons Int (S Z) Z (pf (S Z)) 2 (Nil Int Z (pf Z))); -- <1, 2>
def test1 = head Int Z (tail Int (S Z) vec2); -- head (tail <1, 2>) = 2
def test2 = head Int (S (S Z)) (castdn!^14 (append Int (S Z) (S (S Z)) vec1 vec2) :: Vec Int (S (S (S Z)))); -- head (<3> ++ <1, 2>) = 3
@test1 ++ " " ++ @test2
