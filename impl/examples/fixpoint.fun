--> \(f : * -> *) (a : *) (m : Functor f) (g : f a -> a) (t : Fix f) . g (fmap f m (Fix f) a ((/cata : (f : * -> *) -> (a : *) -> Functor f -> (f a -> a) -> Fix f -> a . \(f : * -> *) (a : *) (m : Functor f) (g : f a -> a) (t : Fix f) . g (fmap f m (Fix f) a (cata f a m g) (out f t))) f a m g) (out f t))
{-

Fixpoints of functors, also define a generic fold function `cata`

-}

data Functor (f : * -> *) = Func {fmap : (a : *) -> (b : *) -> (a -> b) -> f a -> f b}; 
data Fix (f : * -> *) = In {out : (f (Fix f)) }; 
defrec cata : (f : * -> *) -> (a : *) -> Functor f -> (f a -> a) -> Fix f -> a = \f : * -> * . \ a : * . \ m : Functor f . \ g : f a -> a. \ t : Fix f . g (fmap f m (Fix f) a (cata f a m g) (out f t));
cata
