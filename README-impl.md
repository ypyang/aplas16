# Implementation of λI

## Introduction

This is an implementation of `λI` and a simple functional language
**Fun**. Both variants of `λI`, i.e., `λIw` and `λIp` are supported:
`castup`/`castdn` for weak casts and `castup!`/`castdn!` for full
casts. **Fun** supports algebraic data types and pattern matching
using *weak* casts by *Scott encoding* (see Section 5). Bidirectional
type checking is also employed to reduce annotations. This
implementation contains a _REPL_ (Read-Eval-Print-Loop) that
interprets `λI`, as well as a _compiler_ that compiles `λI` to
JavaScript and Haskell.

## Examples

See the [examples directory](impl/examples/)
(first 5 examples are presented in Appendix A).
An [online compiler](https://fun-lang.org/)
is available for running examples in browsers.

Name                                        |  Description
------------------------------------------- | -------------
[hoas.fun    ](impl/examples/hoas.fun    )  | HOAS (A.1)
[functor.fun ](impl/examples/functor.fun )  | Functor record (A.2)
[kindPoly.fun](impl/examples/kindPoly.fun)  | Kind polymorphism (A.3)
[ptree.fun   ](impl/examples/ptree.fun   )  | Labeled binary tree (A.4)
[vector.fun  ](impl/examples/vector.fun  )  | Encoding of vectors (A.5)
[fact.fun    ](impl/examples/fact.fun    )  | Factorial function
[fixpoint.fun](impl/examples/fixpoint.fun)  | Fixpoints of functors
[mutual.fun  ](impl/examples/mutual.fun  )  | Mutual recursion
[polyList.fun](impl/examples/polyList.fun)  | Polymorphic list
[powtree.fun ](impl/examples/powtree.fun )  | Power tree

## Build and run

Files are located at [impl/](impl/). You need to install
[*stack*](https://github.com/commercialhaskell/stack) into PATH and
run the following commands:

```bash
cd impl
make
```

The REPL will run if built successfully. Run `make install` to install
the binary `fun` into `~/.local/bin`.

All examples shown above can be tested by:

```bash
make test
```

## REPL

Type `make` or `fun` (if installed into PATH) **with no arguments** to
invoke the REPL.

Use `tab` for command completion. Type `:?` to show full help. Some
useful commands are:

Command         | Description
-------------   | -------------
`<expr>`        | Evaluate the expression
`:t <expr>`     | Infer the type of expression
`:trans <expr>` | Dump translation of expression
`:l <file>`     | Load and execute the file
`:q`            | Quit the REPL
`:?`            | Show help

## Compiler

Run with arguments to specify the file to compile into JavaScript (or
Haskell), for example

```bash
fun examples/vector.fun      # add "-s" for Haskell
```

If [Node.js](https://nodejs.org/en/) is installed, use `-r` option to
directly run the program after compilation:

```bash
fun -r examples/vector.fun   # add "-s" for Haskell
```

To test all examples by Node.js, run

```bash
cd examples
make
```

Type `fun --help` for more information about the compiler.
 
## Quick Reference

A program consists of declarations separated by semicolon `;` ending
with a *single* expression. Like Haskell, a line comment starts with
`--` and a comment block is wrapped by `{-` and `-}`. Declarations
include datatypes and `def/defrec` bindings:

```
program := decl ; ... ; decl ; expr
decl    := data ... | def ... | defrec ...
```

Here is a short list of supported language features:

+ Literals: 
    * `Int` for non-negative integers
    * `String` for strings
    * `+` for plus, `-` for minus, `**` for multiply (not `*` which is
     reserved for "type-of-type")
    * `++` for string concatenation, `@(e)` for conversion an integer
     to a string
+ Built-in boolean, logical operators and if-else-then clause
+ Annotation: `1 :: Int`
+ Kind expression: `*`, `* -> *`
+ Lambda expression: `\x : Int . x + 1` (__The space after `.` is required!__)
+ Function type: `Int -> Int`
+ Dependent product type: `(f : Int -> *) -> (x : Int) -> f x`
+ Recursive type: `/x . Int -> x :: *`
+ Weak casts: `castdn (castup 3 :: (\x : * . x) Int) :: Int`
+ Full casts: `\f : Int -> *. \x : f (1 + 1) . (castdn! x :: f 2)`
+ Datatype: `data List a = Nil | Cons a (List a);`
+ Record: `data Person = P {age : Int, address : String};`
+ Case expression: `case (Nil Int) of Nil => -1 | Cons x xs => x`
+ Let/letrec local binding: `let id (x : *) = x in id Int`
+ Def/defrec global binding: `defrec f (x : Int) : Int = f x;`
